/* @(#)TranslationPair.java      1.1   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.struct;

/**
 * A [source language LI] -> [target language gloss] object, as found
 * in a bilingual dictionary.
 * 
 * @author liantzelim
 *
 */
public class TranslationPair implements Cloneable {
	private DictSource dict;
	private int id;
	private String srcLang;
	private String tgLang;
	private String lexicalItem;
	private String gloss;

	public TranslationPair(String sl, String tl) {
		this.setSourceLanguage(sl);
		this.setTargetLanguage(tl);
	}

	public TranslationPair(DictSource d, String sl, String tl) {
		this.setDictSource(d);
		this.setSourceLanguage(sl);
		this.setTargetLanguage(tl);
	}
	
	public TranslationPair(DictSource d, int id, String sl, String tl, String li, String g) {
		this.setDictSource(d);
		this.setID(id);
		this.setSourceLanguage(sl);
		this.setTargetLanguage(tl);
		this.setLexicalItem(li);
		this.setGloss(g);
	}
	
	public void setDictSource(DictSource srcLexicon) {
		this.dict = srcLexicon;
	}

	public DictSource getDictSource() {
		return dict;
	}

	public void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return id;
	}

	public String getSourceLanguage() {
		return srcLang;
	}
	public void setSourceLanguage(String srcLang) {
		this.srcLang = srcLang;
	}
	public String getTargetLanguage() {
		return tgLang;
	}
	public void setTargetLanguage(String tgLang) {
		this.tgLang = tgLang;
	}
	public String getLexicalItem() {
		return lexicalItem;
	}
	public void setLexicalItem(String lexicalItem) {
		this.lexicalItem = lexicalItem;
	}
	public String getGloss() {
		return gloss;
	}
	public void setGloss(String gloss) {
		this.gloss = gloss;
	}
	
	public void setTranslationPair(String li, String g) {
		this.setTranslationPair(-1, li, g);
	}
	
	public void setTranslationPair(int id, String li, String g) {
		this.setID(id);
		this.setLexicalItem(li);
		this.setGloss(g);
	}
	
	@Override
	protected Object clone() {
		return new TranslationPair(
				this.dict, this.id, this.srcLang, this.tgLang, 
				this.lexicalItem, this.gloss);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append('[');
		sb.append(this.dict);
		sb.append('#');
		sb.append(this.id);
		sb.append(": ");
		sb.append(this.lexicalItem);
		sb.append(" -> ");
		sb.append(this.gloss);
		sb.append(']');
		return sb.toString();
	}


}
