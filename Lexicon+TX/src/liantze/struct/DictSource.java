/* @(#)DictSource.java      1.1   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.struct;

/**
 * A convenient class for recording details about input bilingual
 * dictionaries.
 * 
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public enum DictSource {
	CEDICT ("cedict", "zho", new String[] {"eng"}),
	SISTEC_EM ("sistecem", "eng", new String[] {"msa"}),
	FEM ("fem", "fra", new String[] {"msa", "eng"}),
	XDICT ("xdict", "eng", new String[] {"zho"}),
	KIMD ("kimd", "eng", new String[] {"msa"}),
	HRIE ("hrie", "iba", new String[] {"eng"}),
	YAITRON_ET ("yaitronET", "eng", new String[] {"tha"}),
	YAITRON_TE ("yaitronTE", "tha", new String[] {"eng"});
	
	String abbrev;
	String srcLanguage;
	String[] destLanguage;
	DictSource (String n, String sl, String[] dl) {
		this.abbrev = n;
		this.srcLanguage = sl;
		this.destLanguage = dl;
	}
	
	public String getAbbrev() {
		return this.abbrev;
	}
}
