/* @(#)TranslationTriple.java      1.0   May 2006
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.struct;

/**
 *
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public class TranslationTriple {
	private String headLI;
	private String tailLI;
	private TranslationPair headLink;
	private TranslationPair midLink;
	private TranslationPair tailLink;
	private double score;
	
	public TranslationTriple(TranslationPair hl, TranslationPair ml, TranslationPair tl) {
		super();
		this.setLang1Lang2Link(hl);
		this.setLang2Lang3Link(ml);
		this.setLang3Lang2Link(tl);
		this.setScore(0.0);
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.headLink.toString());
		sb.append(" === ");
		sb.append(this.midLink.toString());
		sb.append(" === ");
		sb.append(this.tailLink.toString());
		sb.append(" (");
		sb.append(this.score);
		sb.append(')');
		return sb.toString();
	}

	public String getLang1LI() {
		return headLI;
	}
	public String getLang3LI() {
		return tailLI;
	}
	public TranslationPair getLang1Lang2Link() {
		return headLink;
	}
	public void setLang1Lang2Link(TranslationPair headLink) {
//		this.headLink = headLink;
		this.headLink = (TranslationPair) headLink.clone();
		this.headLI = headLink.getLexicalItem();
	}
	public TranslationPair getLang2Lang3Link() {
		return midLink;
	}
	public void setLang2Lang3Link(TranslationPair midLink) {
//		this.midLink = midLink;
		this.midLink = (TranslationPair) midLink.clone();
	}
	public TranslationPair getLang3Lang2Link() {
		return tailLink;
	}
	public void setLang3Lang2Link(TranslationPair tailLink) {
//		this.tailLink = tailLink;
		this.tailLink = (TranslationPair) tailLink.clone();
		this.tailLI = tailLink.getLexicalItem();
	}
	
	public String getLang1() {
		return this.getLang1Lang2Link().getSourceLanguage();
	}
	
	public String getLang2() {
		return this.getLang1Lang2Link().getTargetLanguage();
	}
	
	public String getLang3() {
		return this.getLang2Lang3Link().getTargetLanguage();
	}

	public void setScore(double score) {
		this.score = score;
	}

	public double getScore() {
		return score;
	}
}
