/* @(#)PartOfSpeech.java      1.1   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.struct;

import java.util.HashSet;

/**
 * Convenience class for some common part-of-speech enumerations.
 * 
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public enum PartOfSpeech {
	NOUN,
	VERB,
	ADJECTIVE,
	ADVERB,
	PREPOSITION,
	DETERMINER,
	PRONOUN;
	
	public static HashSet<PartOfSpeech> mapPOS(DictSource dict, String dictPOS) {
		if (dictPOS == null || dict == null) {
			return null;
		}
		
		HashSet<PartOfSpeech> posList = new HashSet<PartOfSpeech>();

		/* XDICT POS */
		if (dict.equals(DictSource.XDICT)) {
			if (dictPOS.trim().equals("n")) {
				posList.add(PartOfSpeech.NOUN);
				posList.add(PartOfSpeech.ADJECTIVE);
			}
			if (dictPOS.trim().charAt(0) == 'v' || dictPOS.trim().equals("pp")) {
				posList.add(PartOfSpeech.VERB);
			}
			if (dictPOS.trim().equals("adj") || dictPOS.trim().equals("a")) {
				posList.add(PartOfSpeech.ADJECTIVE);
				posList.add(PartOfSpeech.NOUN);
			}
			if (dictPOS.trim().equals("ad") || dictPOS.trim().equals("adv")) {
				posList.add(PartOfSpeech.ADVERB);
			}
			if (dictPOS.trim().equals("pron")) {
				posList.add(PartOfSpeech.PRONOUN);
			}
			if (dictPOS.trim().equals("prep")) {
				posList.add(PartOfSpeech.PREPOSITION);
			}
			if (dictPOS.trim().equals("art")) {
				posList.add(PartOfSpeech.DETERMINER);
			}
			return posList;
		}
		
		/* CEDICT POS */
		if (dict.equals(DictSource.CEDICT)) {
			if (dictPOS.trim().equals("N")) {
				posList.add(PartOfSpeech.NOUN);
				posList.add(PartOfSpeech.ADJECTIVE);
			}
			if (dictPOS.trim().equals("V")) {
				posList.add(PartOfSpeech.VERB);
			}
			if (dictPOS.trim().equals("A")) {
				posList.add(PartOfSpeech.ADJECTIVE);
				posList.add(PartOfSpeech.NOUN);
			}
			if (dictPOS.trim().equals("ADV")) {
				posList.add(PartOfSpeech.ADVERB);
			}
			if (dictPOS.trim().equals("DET")) {
				posList.add(PartOfSpeech.DETERMINER);
			}
//			if (dictPOS.trim().equals("PREP")) {
//				return PartOfSpeech.PREPOSITION;
//			}
			if (dictPOS.trim().contains("PRON")) {
				posList.add(PartOfSpeech.PRONOUN);
			}
			return posList;
		} /* END CEDICT POS */
		

		/* BEGIN PTS POS */
		if (dict.equals(DictSource.SISTEC_EM)) {
			if (dictPOS.trim().contains("kn.")) {
				posList.add(PartOfSpeech.NOUN);
				posList.add(PartOfSpeech.ADJECTIVE);
			}
			if (dictPOS.trim().contains("kk.")) {
				posList.add(PartOfSpeech.VERB);
			}
			if (dictPOS.trim().contains("ks.")) {
				posList.add(PartOfSpeech.ADJECTIVE);
				posList.add(PartOfSpeech.NOUN);
			}
			if (dictPOS.trim().matches(".*\\bkt\\.\\b.*")) {
				posList.add(PartOfSpeech.ADVERB);
			}
			if (dictPOS.trim().contains("kgn.")) {
				posList.add(PartOfSpeech.PRONOUN);
			}
			return posList;
		} /* END PTS POS */
		
		
		/* BEGIN HRIE POS */
		if (dict.equals(DictSource.HRIE)) {
			if (dictPOS.trim().equals("N")) {
				posList.add(PartOfSpeech.NOUN);
				posList.add(PartOfSpeech.ADJECTIVE);
			}
			if (dictPOS.trim().equals("VB")) {
				posList.add(PartOfSpeech.VERB);
			}
			if (dictPOS.trim().equals("ADJ")) {
				posList.add(PartOfSpeech.ADJECTIVE);
				posList.add(PartOfSpeech.NOUN);
			}
			if (dictPOS.trim().equals("ADV")) {
				posList.add(PartOfSpeech.ADVERB);
			}
			if (dictPOS.trim().equals("PRON")) {
				posList.add(PartOfSpeech.PRONOUN);
			}
			if (dictPOS.trim().equals("PREP")) {
				posList.add(PartOfSpeech.PREPOSITION);
			}
			return posList;
		}

		/* BEGIN KIMD POS */
		if (dict.equals(DictSource.KIMD)) {
			if (dictPOS.trim().equals("n")) {
				posList.add(PartOfSpeech.NOUN);
				posList.add(PartOfSpeech.ADJECTIVE);
			}
			if (dictPOS.trim().startsWith("v")) {
				posList.add(PartOfSpeech.VERB);
			}
			if (dictPOS.trim().startsWith("adj")) {
				posList.add(PartOfSpeech.ADJECTIVE);
				posList.add(PartOfSpeech.NOUN);
			}
			if (dictPOS.trim().equals("adv")) {
				posList.add(PartOfSpeech.ADVERB);
			}
			if (dictPOS.trim().contains("article")) {
				posList.add(PartOfSpeech.DETERMINER);
			}
			if (dictPOS.trim().contains("pron")) {
				posList.add(PartOfSpeech.PRONOUN);
			}
			return posList;
		} /* END KIMD POS */
		
		/* YAITRON POS */
		if (dict.equals(DictSource.YAITRON_ET) || dict.equals(DictSource.YAITRON_TE)) {
			if (dictPOS.trim().matches(".*\\bn\\b.*")) {
				posList.add(NOUN);
				posList.add(ADJECTIVE);
			}
			if (dictPOS.trim().equals(".*\\badj\\b.*")) {
				posList.add(NOUN);
				posList.add(ADJECTIVE);
			}
			if (dictPOS.trim().equals(".*\\bv\\b.*")) {
				posList.add(VERB);
			}
			if (dictPOS.trim().equals(".*\\badv\\b.*")) {
				posList.add(ADVERB);
			}
			if (dictPOS.trim().equals(".*\\bpron\\b.*")) {
				posList.add(PRONOUN);
			}
			if (dictPOS.trim().equals(".*\\bprep\\b.*")) {
				posList.add(PREPOSITION);
			}

		}

		return null;
	}
}
