/* @(#)ScoredItem.java      1.0   May 2006
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.struct;

/**
 * Convenience class for an object that has a score.
 * 
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public class ScoredItem<T> implements Comparable<ScoredItem<T>> {

	private T item;
	private double score;
	
	public ScoredItem (T t, double s) {
		this.setItem(t);
		this.setScore(s);
	}
	
	public T getItem() {
		return item;
	}
	public void setItem(T item) {
		this.item = item;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}

	public int compareTo(ScoredItem<T> o) {
		if (this.getScore() == o.getScore()) {
			return 0;
		}
		if (this.getScore() < o.getScore()) {
			return -1;
		}
		return 1;
	}
	
}
