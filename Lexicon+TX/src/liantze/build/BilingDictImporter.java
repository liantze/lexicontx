/* @(#)LexiconTXTool.java      1.0   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.build;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * <p>Imports lexical items and glosses from input bilingual dictionary
 * tables into Lexicon+TX LexicalItem and Gloss tables.</p>
 * 
 * <p>Invocation:</p>
 * <code>java liantze.build.BilingDictImporter properties-file [all] [head] [mid] [tail]</code>
 * <ul>
 * <li><code>all</code>: imports entries from all three input dictionaries
 * <li><code>head</code>: imports entries from just "head" dictionary (lang1-lang2)
 * <li><code>mid</code>: imports entries from just "mid" dictionary (lang2-lang3)
 * <li><code>tail</code>: imports entries from just "tail" dictionary (lang3-lang2)
 * </ul>
 * 
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public class BilingDictImporter extends LexiconTXTool {
	private static boolean importHeadDict = false;
	private static boolean importMidDict = false;
	private static boolean importTailDict = false;

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		ResourceBundle bundle = ResourceBundle.getBundle(args[0]);
		initParams(bundle);
		connectDB();
		
		Arrays.sort(args);
//		System.out.println(Arrays.deepToString(args));
		if (Arrays.binarySearch(args, "all") > -1) {
			importHeadDict = true;
			importMidDict = true;
			importTailDict = true;
		}
		if (Arrays.binarySearch(args, "head") > -1) {
			importHeadDict = true;
		}
		if (Arrays.binarySearch(args, "mid") > -1) {
			importMidDict = true;
		}
		if (Arrays.binarySearch(args, "tail") > -1) {
			importTailDict = true;
		}
		
		importDictEntries();
		
		stmt.close();
		dbConn.clearWarnings();

	}

	private static void importDictEntries() throws SQLException {
			importLIs();
			importGlosses();
		}

	private static void importLIs() throws SQLException {
		ResultSet tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM LexicalItem");
		int curLIID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
		tmpRS.close();
		
		String insertLISQL = "INSERT IGNORE INTO LexicalItem (id, lang, LI, pos) VALUES (?,?,?,?)";
		PreparedStatement insertLIStmt = dbConn.prepareStatement(insertLISQL);

		if (importHeadDict) {
			System.out.println("Importing " + lang1_lang2_dict_table);
			// Import lang1 LI
			insertLIStmt.setString(2, lang1);
			tmpRS = stmt.executeQuery(
					"SELECT DISTINCT " + lang1_lang2_LI_col + ", stdPOS " +
							"FROM `" + lang1_lang2_dict_table + "` G " +
							"LEFT JOIN " + lang1_lang2_DictPrefix + "POS P ON (G." + lang1_lang2_pos_col + " = P.pos)");
			while (tmpRS.next()) {
				insertLIStmt.setInt(1, ++curLIID);
				insertLIStmt.setString(3, tmpRS.getString(lang1_lang2_LI_col));
				if (tmpRS.getString("stdPOS") != null) {
					// may have multiple POS
//					String [] posList = tmpRS.getString("stdPOS").split("[|()]+");
//					for (String p : posList) {
						insertLIStmt.setString(4, tmpRS.getString("stdPOS"));
//					}
				} else {
					insertLIStmt.setNull(4, Types.VARCHAR);
				}
				insertLIStmt.executeUpdate();					
			}
			tmpRS.close();
		}

		if (importMidDict) {
			// import lang2 LI
			System.out.println("Importing " + lang2_lang3_dict_table);
			insertLIStmt.setString(2, lang2);
			tmpRS = stmt.executeQuery(
					"SELECT DISTINCT " + lang2_lang3_LI_col + ", stdPOS " +
							"FROM `" + lang2_lang3_dict_table + "` G " +
							"LEFT JOIN " + lang2_lang3_DictPrefix + "POS P ON (G." + lang2_lang3_pos_col + " = P.pos)");
			while (tmpRS.next()) {
				insertLIStmt.setInt(1, ++curLIID);
				insertLIStmt.setString(3, tmpRS.getString(lang2_lang3_LI_col));
				if (tmpRS.getString("stdPOS") != null) {
					// may have multiple POS
//					String [] posList = tmpRS.getString("stdPOS").split("[|()]+");
//					for (String p : posList) {
						insertLIStmt.setString(4, tmpRS.getString("stdPOS"));
//					}
				} else {
					insertLIStmt.setNull(4, Types.VARCHAR);
				}
				insertLIStmt.executeUpdate();					
			}
			tmpRS.close();
		}

		if (importTailDict) {
			// import lang3 LI
			System.out.println("Importing " + lang3_lang2_dict_table);
			insertLIStmt.setString(2, lang3);
			tmpRS = stmt.executeQuery(
					"SELECT DISTINCT " + lang3_lang2_LI_col + ", stdPOS " +
							"FROM `" + lang3_lang2_dict_table + "` G " +
							"LEFT JOIN " + lang3_lang2_DictPrefix + "POS P ON (G." + lang3_lang2_pos_col + " = P.pos)");
			while (tmpRS.next()) {
				insertLIStmt.setInt(1, ++curLIID);
				insertLIStmt.setString(3, tmpRS.getString(lang3_lang2_LI_col));
				if (tmpRS.getString("stdPOS") != null) {
					// may have multiple POS
//					String [] posList = tmpRS.getString("stdPOS").split("[|()]+");
//					for (String p : posList) {
						insertLIStmt.setString(4, tmpRS.getString("stdPOS"));
//					}
				} else {
					insertLIStmt.setNull(4, Types.VARCHAR);
				}
				insertLIStmt.executeUpdate();					
			}
			tmpRS.close();
		}
	}

	private static void importGlosses() throws SQLException {
		ResultSet tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM Gloss");
		int curGlossID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
		tmpRS.close();

		String insertGlossSQL = "INSERT INTO Gloss " +
				"(id, liId, lang, srcDict, oriPos, glossStr, pos) " +
				"VALUES (?,?,?,?,?,?,?)";
		PreparedStatement insertGlossStmt = dbConn.prepareStatement(insertGlossSQL);

		if (importHeadDict) {
			// Add lang2 glosses from lang1_lang2_dict
			insertGlossStmt.setString(3, lang2);
			String tmp ="SELECT DISTINCT LexicalItem.id liId, g." + lang1_lang2_pos_col + " pos, stdPOS, " +
					lang1_lang2_gloss_id_col + " gid, g." + lang1_lang2_gloss_col  + " gloss " +
					"FROM `" + lang1_lang2_dict_table + "` g " +
					"LEFT JOIN " + lang1_lang2_DictPrefix + "POS " +
					"ON (g." + lang1_lang2_pos_col + " = " + lang1_lang2_DictPrefix + "POS.pos) " + // this and following lines Added 16 Sept 2011
					"LEFT JOIN LexicalItem ON (g." + lang1_lang2_LI_col + " = LexicalItem.li " +
					"AND (" + lang1_lang2_DictPrefix + "POS.stdPOS = LexicalItem.pos " +
					"OR LexicalItem.pos IS NULL)) " +
					"WHERE lang='" + lang1 + "'";
			//		System.out.println(tmp);
			tmpRS = stmt.executeQuery(tmp);
			while (tmpRS.next()) {
				insertGlossStmt.setInt(1, ++curGlossID);
				insertGlossStmt.setInt(2, tmpRS.getInt("liId"));
				insertGlossStmt.setString(4, lang1_lang2_DictPrefix + "#" + tmpRS.getInt("gid"));
				insertGlossStmt.setString(5, tmpRS.getString("pos"));
				insertGlossStmt.setString(6, tmpRS.getString("gloss"));
				insertGlossStmt.setString(7, tmpRS.getString("stdPos"));
				insertGlossStmt.executeUpdate();
			}
			tmpRS.close();
		}

		if (importMidDict) {
			// Add lang3 glosses from lang2_lang3_dict
			insertGlossStmt.setString(3, lang3);
			tmpRS = stmt.executeQuery(
					"SELECT DISTINCT LexicalItem.id liId, g." + lang2_lang3_pos_col + " pos, stdPOS, " +
							lang2_lang3_gloss_id_col + " gid, g." + lang2_lang3_gloss_col  + " gloss " +
							"FROM `" + lang2_lang3_dict_table + "` g " +
							"LEFT JOIN " + lang2_lang3_DictPrefix + "POS " +
							"ON (g." + lang2_lang3_pos_col + " = " + lang2_lang3_DictPrefix + "POS.pos) " + // this and following lines Added 16 Sept 2011
							"LEFT JOIN LexicalItem ON (g." + lang2_lang3_LI_col + " = LexicalItem.li " +
							"AND (" + lang2_lang3_DictPrefix + "POS.stdPOS = LexicalItem.pos " +
							"OR LexicalItem.pos IS NULL)) " +
							"WHERE lang='" + lang2 + "'" );
			while (tmpRS.next()) {
				insertGlossStmt.setInt(1, ++curGlossID);
				insertGlossStmt.setInt(2, tmpRS.getInt("liId"));
				insertGlossStmt.setString(4, lang2_lang3_DictPrefix + "#" + tmpRS.getInt("gid"));
				insertGlossStmt.setString(5, tmpRS.getString("pos"));
				insertGlossStmt.setString(6, tmpRS.getString("gloss"));
				insertGlossStmt.setString(7, tmpRS.getString("stdPos"));
				insertGlossStmt.executeUpdate();
			}
			tmpRS.close();
		}

		if (importTailDict) {
			// Add lang2 glosses from lang3_lang2_dict
			insertGlossStmt.setString(2, lang2);
			tmpRS = stmt.executeQuery(
					"SELECT DISTINCT LexicalItem.id liId, g." + lang3_lang2_pos_col + " pos, stdPOS, " +
							lang3_lang2_gloss_id_col + " gid, g." + lang3_lang2_gloss_col  + " gloss " +
							"FROM `" + lang3_lang2_dict_table + "` g " +
							"LEFT JOIN " + lang3_lang2_DictPrefix + "POS " +
							"ON (g." + lang3_lang2_pos_col + " = " + lang3_lang2_DictPrefix + "POS.pos) " + // this and following lines Added 16 Sept 2011
							"LEFT JOIN LexicalItem ON (g." + lang3_lang2_LI_col + " = LexicalItem.li " +
							"AND (" + lang3_lang2_DictPrefix + "POS.stdPOS = LexicalItem.pos " +
							"OR LexicalItem.pos IS NULL)) " +
							"WHERE lang='" + lang3 + "'" );
			while (tmpRS.next()) {
				insertGlossStmt.setInt(1, ++curGlossID);
				insertGlossStmt.setInt(2, tmpRS.getInt("liId"));
				insertGlossStmt.setString(4, lang3_lang2_DictPrefix + "#" + tmpRS.getInt("gid"));
				insertGlossStmt.setString(5, tmpRS.getString("pos"));
				insertGlossStmt.setString(6, tmpRS.getString("gloss"));
				insertGlossStmt.setString(7, tmpRS.getString("stdPos"));
				insertGlossStmt.executeUpdate();
			}
			tmpRS.close();
		}
	}

}
