/* @(#)TripleGenerator.java      1.2   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.build;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import liantze.struct.PartOfSpeech;
import liantze.struct.ScoredItem;
import liantze.struct.TranslationPair;
import liantze.struct.TranslationTriple;
import liantze.util.DictUtils;
import liantze.util.FileUtils;

/**
 * <p>Implementation of the modified one-time inverse consultation proceduce.
 * Given bilingual dictionaries for language pairs L1-L2, L2-L3 and L3-L2,
 * We generate translation triples for (L1, L2, L3).</p>
 * <p>(Need I repeat the admonishment again that THIS IS GRAD SCHOOL CODE?</p>
 * 
 * <p>Invocation:</p>
 * <code>java liantze.build.TripleGenerator properties-file [filterPOS]</code>
 * 
 * <p><code>filterPos</code> is recommended for removing most frivolous mappings.</p>
 * @author Lim Lian Tze (liantze@gmail.com)
 *
 */
public class TripleGenerator extends LexiconTXTool {
	static boolean debug;
	static boolean filterPOS;
	static int tripleID = 0;
	static int cluster1ID = 0;

	
	public static void main (String args[]) throws SQLException {
		ResourceBundle bundle = ResourceBundle.getBundle(args[0]);
		if (args.length > 1 && args[1].toLowerCase().equals("filterpos")) {
			filterPOS = true;
		} else {
			filterPOS = false;
		}
		initParams(bundle);
		connectDB();
		
		logger = FileUtils.getUTF8Writer(
				logPrefix + "-gentriple-" + System.currentTimeMillis()/1000 + ".log");
		
		initTables(); // create necessary tables
		align(); // generate triples and put them in triplesTableName
		
		// Cleaning up
		stmt.close();
		addTripleStmt.close();
//		addCluster1Stmt.close();
		dbConn.close();
		logger.close();
}


	/**
	 * Create necessary tables
	 * @throws SQLException
	 */
	private static void initTables() throws SQLException {
		stmt.execute("DROP TABLE IF EXISTS `" + triplesPrefix +  "_temp`");
		
		stmt.execute("CREATE TABLE IF NOT EXISTS `"+ triplesPrefix +"_temp` (" +
				"`id` int(11) PRIMARY KEY, " +
				"`cluster1_id` int(11) NOT NULL, " +
				"`" + lang1_lang2_DictPrefix + "_id` int(11) NOT NULL, " +
				"`" + lang2_lang3_DictPrefix + "_id` int(11) NOT NULL, " +
				"`" + lang3_lang2_DictPrefix + "_id` int(11) NOT NULL, " +
				"`chain_score` double NOT NULL DEFAULT '0', " +
				"`cluster1_score` double DEFAULT '0', " +
				"INDEX `idx_" + triplesPrefix + "TEMP_c1` (cluster1_id), " +
				"CONSTRAINT `fk_" + triplesPrefix + "_temp_" + lang1_lang2_DictPrefix + "` FOREIGN KEY (`"+ lang1_lang2_DictPrefix + "_id`) REFERENCES `" + lang1_lang2_dict_table + "` (" + lang1_lang2_gloss_id_col + ") ON DELETE NO ACTION ON UPDATE NO ACTION, " +
				"CONSTRAINT `fk_" + triplesPrefix + "_temp_" + lang2_lang3_DictPrefix + "` FOREIGN KEY (`"+ lang2_lang3_DictPrefix + "_id`) REFERENCES `" + lang2_lang3_dict_table + "` (" + lang2_lang3_gloss_id_col + ") ON DELETE NO ACTION ON UPDATE NO ACTION, " +
				"CONSTRAINT `fk_" + triplesPrefix + "_temp_" + lang3_lang2_DictPrefix + "` FOREIGN KEY (`"+ lang3_lang2_DictPrefix + "_id`) REFERENCES `" + lang3_lang2_dict_table + "` (" + lang3_lang2_gloss_id_col + ") ON DELETE NO ACTION ON UPDATE NO ACTION " +
				") ENGINE=InnoDB DEFAULT CHARSET=utf8" );

		
		lang1_lang2_Query = dbConn.prepareStatement(lang1_lang2_qstr);
		lang2_lang3_Query = dbConn.prepareStatement(lang2_lang3_qstr);
		lang3_lang2_Query = dbConn.prepareStatement(lang3_lang2_qstr);
		
		addTripleStmt = dbConn.prepareStatement(
				"INSERT INTO `" + triplesPrefix + "_temp` " +
				"(id, cluster1_id, " + 
				lang1_lang2_DictPrefix + "_id, " + lang2_lang3_DictPrefix + "_id, " + lang3_lang2_DictPrefix + "_id, " +
				"chain_score, cluster1_score) " + 
				"VALUES (?, ?, ?, ?, ?, ?, ?)");

	}
	
	/**
	 * Implementation of the modified OTIC procedure. Probably buggy. T_T
	 * @throws SQLException
	 */
	private static void align() throws SQLException {
		// Initialisations
		stmt.execute("TRUNCATE `" + triplesPrefix + "_temp`");
		

		ResultSet  lang1_lang2_RS = null, lang2_lang3_RS = null, lang3_lang2_RS = null;
		ResultSet all_lang1_LIs = stmt.executeQuery(get_lang1_LI_qstr);
		int count = 0;
		while (all_lang1_LIs.next()  
				//&& ++count < 2*/ //run for a few small items to test
				) {

			// select a lang1 word
			String lang1_LI = all_lang1_LIs.getString(lang1_lang2_LI_col);
//			System.out.println("Looking at lang1 " + lang1_LI);
			HashSet<PartOfSpeech> lang1_POS, lang2_POS, lang3_POS;
			String lang2_LI, lang3_LI;
			String lang2_fw_Gloss, lang2_inv_gloss, lang3_gloss;
			int lang1_lang2_ID, lang2_lang3_ID, lang3_lang2_ID;
			Set<String> lang2_fw_set = new HashSet<String>();
			Set<String> lang2_inv_set = new HashSet<String>();
			Set<String> lang3_set = new HashSet<String>();
			
			// Translation triples are indexed by lang1-lang3.
			// Read the algorithm or the paper and you'll see why.
			Map<String, Set<TranslationTriple>> triples= new HashMap<String, Set<TranslationTriple>>();
			TranslationPair lang1_lang2_pair, lang2_lang3_pair, lang3_lang2_pair;


			debug = false;
			// get all lang1 -> lang2 TranslationPairs from lang1_lang2_Dict
			lang1_lang2_Query.setString(1, lang1_LI);
			lang1_lang2_RS = lang1_lang2_Query.executeQuery();


			// for each lang1 -> lang2 TranslationPair,
			while (lang1_lang2_RS.next()){
				lang2_fw_Gloss = lang1_lang2_RS.getString(lang1_lang2_gloss_col);
				lang1_lang2_ID = lang1_lang2_RS.getInt(lang1_lang2_gloss_id_col);
				
				// TODO 
				// you may or may not need to implement normalisation for lang2
				// to get a more "lemma"-like form
				lang2_LI = DictUtils.normalise(lang2, lang2_fw_Gloss);
//				System.out.println("Forward lang2_LI = " + lang2_LI);
				
				lang1_POS = PartOfSpeech.mapPOS(lang1_lang2_Dict, 
						lang1_lang2_RS.getString(lang1_lang2_pos_col));

				// add lang2_LI to lang2_fw_set Hashset
				lang2_fw_set.add(lang2_LI);

				// and create the head TranslationPair
				lang1_lang2_pair = new TranslationPair(
						lang1_lang2_Dict, lang1_lang2_ID, lang1, lang2, 
						lang1_LI, lang2_fw_Gloss);
				log("Created " + lang1_lang2_pair.toString());

				
				// use the normalised lang2 to get lang2-lang3 TranslationPairs
				// from lang2_lang3_dict
				lang2_lang3_Query.setString(1, lang2_LI);
				lang2_lang3_RS = lang2_lang3_Query.executeQuery();

				// for each lang2_lang3 TranslationPair
				while (lang2_lang3_RS.next()){
					
					lang2_POS = PartOfSpeech.mapPOS(lang2_lang3_Dict, 
							lang2_lang3_RS.getString(lang2_lang3_pos_col));
					lang3_gloss = lang2_lang3_RS.getString(lang2_lang3_gloss_col);
					lang2_lang3_ID = lang2_lang3_RS.getInt(lang2_lang3_gloss_id_col);
					if (filterPOS) {
						if (lang1_POS != null && lang2_POS != null) {
//							if (debug) {
//								log(lang1_POS + " <-> " + lang2_POS);
//							}
							lang2_POS.retainAll(lang1_POS);
							if (lang2_POS.isEmpty()) {
//								if (debug) {
//									log ("Skipping " + lang3_gloss);
//								}
								continue;
							}
						}
					}
						
					
					// TODO 
					// you may or may not need to implement normalisation for lang3
					// to get a more "lemma"-like form
					lang3_LI = DictUtils.normalise(lang3, lang3_gloss);
//					System.out.println("lang3_LI = " + lang3_LI);

					// add the normalised lang3_LI to lang3_set HashSet
					lang3_set.add(lang3_LI);

					// and create the mid TranslationPair
					lang2_lang3_pair = new TranslationPair(
							lang2_lang3_Dict, lang2_lang3_ID, lang2, lang3, lang2_LI, lang3_gloss);
					log("\tCreated " + lang2_lang3_pair.toString());

					// use normalised lang3_LI to get the lang3_lang2 TranslationPairs
					// from lang3_lang2_Dict (inverse lookup)
					lang3_lang2_Query.setString(1, lang3_LI);
					lang3_lang2_RS = lang3_lang2_Query.executeQuery();

					// for each lang3-lang2 TranslationPair
					while (lang3_lang2_RS.next()){
						lang2_inv_gloss = lang3_lang2_RS.getString(lang3_lang2_gloss_col);
						lang3_lang2_ID = lang3_lang2_RS.getInt(lang3_lang2_gloss_id_col);
						lang3_POS = PartOfSpeech.mapPOS(lang3_lang2_Dict, 
								lang3_lang2_RS.getString(lang3_lang2_pos_col));
						if (filterPOS) {
							if (lang1_POS != null && lang3_POS != null) {
//								if (debug) {
//									log ("\t" + lang1_POS + " <-> " + lang3_POS);
//								}
								lang3_POS.retainAll(lang1_POS);
								if (lang3_POS.isEmpty()) {
//									if (debug) {
//										log("\tSkipping " + lang2_inv_gloss);
//									}
									continue;
								}
							}
						}
						
						
//						System.out.println("Reverse Lang2 LI = " + lang2_inv_gloss);

						lang3_lang2_pair = new TranslationPair(
								lang3_lang2_Dict, lang3_lang2_ID, lang3, lang2, lang3_LI, lang2_inv_gloss);
						log("\t\tCreated " + lang3_lang2_pair.toString());

						// construct a TranslationTriple comprising current
						// lang1_lang2, lang2_lang3, lang_3lang2
						String key = lang1_LI + " <--> " + lang3_LI;
						if (!triples.containsKey(key)) {
							triples.put(key, new HashSet<TranslationTriple>());
						}
						triples.get(key).add(new TranslationTriple(
								lang1_lang2_pair, lang2_lang3_pair, lang3_lang2_pair));

					} // end for each lang3_lang2
				} // end for each lang2_lang3
			} // end for each lang1_lang2


			debug = false;
			log("\nDebug: triples pool now contains: ");
			for (String z : triples.keySet()) {
				log(z);
				for (TranslationTriple c: triples.get(z)) {
					log("\t" + c.toString());
				}
			}
			
			log("");

			// OK! COMPARING TIME!!!
			// for each lang1_LI sub-group:		
			debug = false;
			List<ScoredItem<String>> scoreList = new ArrayList<ScoredItem<String>>();
			for (String z : triples.keySet()){
				log(z);

				double tripleScore = 0.0;
				double tripleScoreTotal = 0.0;
				double finalScore = 0.0;
				lang2_inv_set = new HashSet<String>();

				// for each TranslationTriple
				for (TranslationTriple c: triples.get(z)){
					String lang2_inv_LI = 
							DictUtils.normalise(lang2, c.getLang3Lang2Link().getGloss());

					boolean added = lang2_inv_set.add(lang2_inv_LI);
					// compare the normalised lang2_inv_LI gloss of lang3-lang2 against lang2_fw Hashset
					// add normalised lang2 gloss of lang3-lang2 into lang2_inv Hashset
					// Test A: exact match only
					// Test B: implement partial matching (*currently used)
					// aaaaanyway get a subscore for each TranslationChain
					// and get totalScore running total
					tripleScore = getScore(lang2_inv_LI, lang2_fw_set);
					c.setScore(tripleScore);
					log(c.toString());
					if (added) {
						tripleScoreTotal += tripleScore;
					}

				} // end for each TranslationTriple

				finalScore = 2.0 * tripleScoreTotal / (lang2_fw_set.size() + lang2_inv_set.size());
				log("FORWARD:");
				for (String s : lang2_fw_set) {
					log('\t' + s);
				}
				log("BACKWARD:");
				for (String s : lang2_inv_set) {
					log('\t' + s);
				}
				// in fact need to create a list of lang1LI--score pairings so that can be sorted!
				scoreList.add(new ScoredItem<String>(z, finalScore));
				log("score for " + z + " = " + finalScore + '\n');
			} // end for each lang1LI sub-group


			debug = false;
			log("\nDebug: chain pool now contains: ");
			for (String z : triples.keySet()) {
				log(z);
				for (TranslationTriple c: triples.get(z)) {
					log("\t" + c.toString());
				}
			}


			// re-rank lang3 sub-groups according to score(lang3LI)
			debug = true;
			Collections.sort(scoreList, Collections.reverseOrder());

			// output TranslationTriples in each lang3 sub-group
			//		verboseReport(chains, scoreList);
			recordTriple(triples, scoreList);

		} // end all...

		// cleaning up
		all_lang1_LIs.close();
		lang3_lang2_RS.close();
		lang2_lang3_RS.close();
		lang1_lang2_RS.close();
		lang3_lang2_Query.close();
		lang2_lang3_Query.close();
		lang1_lang2_Query.close();
//		addCluster1Stmt.close();
		addTripleStmt.close();
	}
	

	// populate chains and initial clusters in Database
	private static void recordTriple(Map<String, Set<TranslationTriple>> chains,
			List<ScoredItem<String>> scoreList) throws SQLException {
		// well if nothing's available, might as well call it a day now.
		if (chains == null || chains.isEmpty() || scoreList == null || scoreList.isEmpty()) {
			return;
		}
		
		StringBuilder sb;
		Set<String> simpleChains = new HashSet<String>();

		for (ScoredItem<String> si : scoreList) {
			if (si.getScore() == 0) {
				continue;
			}


			// Log each group
			log("\"" + si.getItem().replaceAll(" <--> ", "\" <--> \"") +"\"" + " (" + si.getScore() + ")");

			// set cluster ID for chains in this group and log to file
			addTripleStmt.setInt(2, ++cluster1ID);

			for (TranslationTriple c: chains.get(si.getItem())) {

				// skip chains with 0 score
				if (c.getScore() == 0) {
					continue;
				}

				sb = new StringBuilder();
				sb.append("Triple> ");
				sb.append(c.getLang1Lang2Link().getID());
				sb.append('#');
				sb.append(c.getLang1LI());
				sb.append('\t');
				sb.append(c.getLang2Lang3Link().getID());
				sb.append('#');
				sb.append(c.getLang2Lang3Link().getLexicalItem());
				sb.append('\t');
				sb.append(c.getLang3Lang2Link().getID());
				sb.append('#');
				sb.append(c.getLang3LI());
				sb.append('\t');
				sb.append(c.getScore());

				if (simpleChains.add(sb.toString())) {
					log(sb.toString());
					
					// and add to database, too!
					addTripleStmt.setInt(1, ++tripleID);
					addTripleStmt.setInt(3, c.getLang1Lang2Link().getID());
					addTripleStmt.setInt(4, c.getLang2Lang3Link().getID());
					addTripleStmt.setInt(5, c.getLang3Lang2Link().getID());
					addTripleStmt.setDouble(6, c.getScore());
					addTripleStmt.setDouble(7, si.getScore());
					addTripleStmt.executeUpdate();
				}
			}
			log("");
		}
	}

	private static void log(String s) {
		if (debug) {
			logger.println(s);
		}
	}

	// TODO you can either award score based on exact match or partial match
	private static double getScore(String s, Set<String> pool) {
		//		return getExactMatchScore(s, pool);
		return getPartialMatchScore(s, pool);
	}

	private static double getExactMatchScore(String s, Set<String> pool) {
		if (pool.contains(s)) {
			return 1d;
		}
		return 0d;
	}

	private static double getPartialMatchScore(String s, Set<String> pool) {
		// is there an exact match? if yes return 1.0 and we're done
		if (getExactMatchScore(s, pool) > 0) {
			return 1d;
		}

		// OK need to go through each string in pool (ps)
		// Bear in mind that pool contains LI and s is a gloss, so length(s) > length(ps)
		double score = 0d;
		String[] stoks = s.split("[ ()-]");
		int toksCount = stoks.length;
		List<String> stoksList = new ArrayList<String>(Arrays.asList(stoks));
		for (String ps : pool){

			// ps not \subsequence s? skip.
			if (!s.contains(ps)) {
				continue;
			}

			// subsequence matching
			String[] pstoks = ps.split(" ()-");
			List<String> pstoksList = new ArrayList<String>(Arrays.asList(pstoks));
			pstoksList.retainAll(stoksList);
			score += (double)(pstoksList.size()) / toksCount;
		}
		return score;
	}

}
