/* @(#)LexiconExpander.java      1.3   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.build;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * <p>Filters translation triples based on some threshold value, then
 * regroups as translation sets in new multilingual lexicon or adds
 * to existing translation sets in existing multilingual lexicon.</p>
 * 
 * <p>Invocation:</p>
 * <code>java liantze.build.LexiconExpander properties-file [create-new | add-lang]</code>
 * 
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public class LexiconExpander extends LexiconTXTool {
	
	static boolean lexiconExists;
	
	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		ResourceBundle bundle = ResourceBundle.getBundle(args[0]);
		if (args.length > 1) {
			if (args[1].equals("create-new")) {
				lexiconExists = false;
			} else if (args[1].equals("add-lang")) {
				lexiconExists = true;
			} else {
				System.err.println("Operation " + args[1] + " unrecogised. Valid operations are: ");
				System.err.println("\t" + "create-new");
				System.err.println("\t" + "add-lang");
				System.err.println("Aborting.");
				System.exit(1);
			}
		} else {
			System.err.println("No operation was specified. Please rerun with ");
			System.err.println("\t java liantze.build.LexiconExpander <properties filename> <operation>");
			System.err.println("Valid operations are: ");
			System.err.println("\t" + "create-new");
			System.err.println("\t" + "add-lang");
			System.err.println("Aborting.");
			System.exit(1);			
		}
		initParams(bundle);
		connectDB();


		createTables();
		filter(); // import only triples belonging to sets with scores >= threshold
		checkMidLang(); // try to make sure each middle language (Lang2) LI is included at least once
		checkTailLang(); // try to make sure each tail language (Lang3) LI is included at least once
		if (lexiconExists) { // if a multilingual lexicon already exists, update first
			updateExistingSets();
			System.out.println("Stopped. Clustering remaining triples now.");
		}

		clusterSets(); // generate sets from new triples
		importAxis();  // copy translation sets into Lexicon+TX

		shutdown();
	}
	
	
	//	private static void log(String s) {
	//		if (debug) {
	////			System.out.println(s);
	//			logger.println(s);
	//		}
	//	}
		
		private static void createTables() throws SQLException {
			stmt.execute("DROP TABLE IF EXISTS `" + triplesPrefix + "_triple`");
			
			String createTripleSQL = "CREATE TABLE IF NOT EXISTS `"+ triplesPrefix +"_triple` (" +
					"`id` int(11) PRIMARY KEY, " +
					"`cluster1_id` int(11), " +
					"`cluster2_id` int(11), " +
					"`cluster3_id` int(11), " +
					"`" + lang1_lang2_DictPrefix + "_id` int(11) NOT NULL, " +
					"`" + lang2_lang3_DictPrefix + "_id` int(11) NOT NULL, " +
					"`" + lang3_lang2_DictPrefix + "_id` int(11) NOT NULL, " +
					"`triple_score` double NOT NULL DEFAULT '0', " +
					"`cluster1_score` double DEFAULT '0', " +
					"`otic_score` double DEFAULT '0', " +
					"INDEX (cluster1_id), INDEX (cluster2_id), INDEX (cluster3_id), " +
					"CONSTRAINT `fk_" + triplesPrefix + "Triple_" + lang1_lang2_DictPrefix + "` FOREIGN KEY (`"+ lang1_lang2_DictPrefix + "_id`) REFERENCES `" + lang1_lang2_dict_table + "` (`" + lang1_lang2_gloss_id_col + "`) ON DELETE NO ACTION ON UPDATE NO ACTION, " +
					"CONSTRAINT `fk_" + triplesPrefix + "Triple_" + lang2_lang3_DictPrefix + "` FOREIGN KEY (`"+ lang2_lang3_DictPrefix + "_id`) REFERENCES `" + lang2_lang3_dict_table + "` (`" + lang2_lang3_gloss_id_col + "`) ON DELETE NO ACTION ON UPDATE NO ACTION, " +
					"CONSTRAINT `fk_" + triplesPrefix + "Triple_" + lang3_lang2_DictPrefix + "` FOREIGN KEY (`"+ lang3_lang2_DictPrefix + "_id`) REFERENCES `" + lang3_lang2_dict_table + "` (`" + lang3_lang2_gloss_id_col + "`) ON DELETE NO ACTION ON UPDATE NO ACTION " +
					") ENGINE=InnoDB DEFAULT CHARSET=utf8";
			stmt.execute(createTripleSQL);
		}


	private static void filter() throws SQLException {
			String tmp = "SELECT MAX(cluster1_score) max_score " +
					"FROM `" + triplesPrefix + "_temp` INNER JOIN `" + lang1_lang2_dict_table + "` m " +
					"ON (`" + lang1_lang2_DictPrefix + "_id` = m." + lang1_lang2_gloss_id_col + ") WHERE " + 
					lang1_lang2_LI_col + " = ?";
	//		System.out.println(tmp);
			PreparedStatement getScoreStmt = dbConn.prepareStatement(tmp);
			PreparedStatement filterTripleStmt = dbConn.prepareStatement(
					"INSERT INTO `" + triplesPrefix + "_triple` (id, " +
					lang1_lang2_DictPrefix + "_id, " + lang2_lang3_DictPrefix + "_id, " + lang3_lang2_DictPrefix + "_id, " +
							"cluster1_id, triple_score, otic_score) " +
					"SELECT c.id, " +
						"c." + lang1_lang2_DictPrefix + "_id, " +
						"c." + lang2_lang3_DictPrefix + "_id, " +
						"c." + lang3_lang2_DictPrefix + "_id, " +
						"cluster1_id, chain_score, cluster1_score " +
						"FROM `" + triplesPrefix + "_temp` c INNER JOIN `" + lang1_lang2_dict_table + "` m " +
						"ON (`" + lang1_lang2_DictPrefix + "_id` = m." + lang1_lang2_gloss_id_col + ") " +
						"WHERE " + lang1_lang2_LI_col + " = ? " +
						"AND (POW(cluster1_score, 2) >= ? OR cluster1_score >= ?)");
			
			// get all distinct lang1 LIs from xxx_temp
			ResultSet headLIs = stmt.executeQuery(
					"SELECT DISTINCT " + lang1_lang2_LI_col +
					" FROM `" + triplesPrefix + "_temp` INNER JOIN `" + lang1_lang2_dict_table + "` m " +
					"ON (" + lang1_lang2_DictPrefix + "_id = " + lang1_lang2_gloss_id_col + ") ORDER BY cluster1_id");
			
			
			// for each one get the max cluster1_score, calculate thresholds
			while (headLIs.next()) {
				String headLI = headLIs.getString(lang1_lang2_LI_col);
				getScoreStmt.setString(1, headLI);
				ResultSet maxScoreResult = getScoreStmt.executeQuery();
				if (maxScoreResult.next()) {
					double max = maxScoreResult.getDouble("max_score");
					double squaredThreshold = max * threshold_BETA;
					double normalThreshold = max * threshold_ALPHA;
					// insert into triples table chains that are acceptable
					filterTripleStmt.setString(1, headLI);
					filterTripleStmt.setDouble(2, squaredThreshold);
					filterTripleStmt.setDouble(3, normalThreshold);
					filterTripleStmt.executeUpdate();
				}
				maxScoreResult.close();
			}
			
			headLIs.close();
			filterTripleStmt.close();
			getScoreStmt.close();
			
		}


	private static void checkMidLang() throws SQLException {
		// get list of all lang2 words in raw triples
		// check if they are already included in triple table?
		ResultSet missingMidLIs = stmt.executeQuery(
				"SELECT DISTINCT e." + lang2_lang3_LI_col + " LI " +
				"FROM `" + triplesPrefix + "_temp` c INNER JOIN `" +
				lang2_lang3_dict_table + "` e ON (" + 
				lang2_lang3_DictPrefix+ "_id = e." + lang2_lang3_gloss_id_col + ") " +
				"LEFT OUTER JOIN " +
				"(" + triplesPrefix + "_triple t INNER JOIN `" + lang2_lang3_dict_table + "` te " +
				"ON (" + lang2_lang3_DictPrefix + "_id = te." + lang2_lang3_gloss_id_col + ")) " +
				"ON e." + lang2_lang3_LI_col + " = te." + lang2_lang3_LI_col + " WHERE te." + lang2_lang3_LI_col + " IS NULL");
		
		// if not, include at least one chain in chain in triple table
		PreparedStatement insertStmt = dbConn.prepareStatement(
				"INSERT INTO `" + triplesPrefix + "_triple` " +
				"(id, " + lang1_lang2_DictPrefix + "_id, " + lang2_lang3_DictPrefix + "_id, " + lang3_lang2_DictPrefix + "_id, cluster1_id, triple_score, otic_score) " +
				"SELECT c.id, c." + lang1_lang2_DictPrefix + "_id, c." + lang2_lang3_DictPrefix + "_id, c." + lang3_lang2_DictPrefix + "_id, cluster1_id, chain_score, cluster1_score " +
				"FROM `" + triplesPrefix + "_temp` c INNER JOIN `" +
				lang2_lang3_dict_table + "` e ON (" + 
				lang2_lang3_DictPrefix + "_id = e." + lang2_lang3_gloss_id_col + ") " + 
				"WHERE " + lang2_lang3_LI_col + " = ? " +
				"ORDER BY cluster1_score desc LIMIT 1");
		
		while (missingMidLIs.next()) {
			insertStmt.setString(1, missingMidLIs.getString("LI"));
			insertStmt.executeUpdate();
		}
		
		insertStmt.close();
		missingMidLIs.close();
	}


	private static void checkTailLang() throws SQLException {
		// get list of all lang2 words in raw triples
		// check if they are already included in triple table?
		ResultSet missingTailLIs = stmt.executeQuery(
				"SELECT DISTINCT e." + lang3_lang2_LI_col + " LI " +
				"FROM `" + triplesPrefix + "_temp` c INNER JOIN `" +
				lang3_lang2_dict_table + "` e ON (" + 
				lang3_lang2_DictPrefix+ "_id = e." + lang3_lang2_gloss_id_col + ") " +
				"LEFT OUTER JOIN " +
				"(" + triplesPrefix + "_triple t INNER JOIN `" + lang3_lang2_dict_table + "` te " +
				"ON (" + lang3_lang2_DictPrefix + "_id = te." + lang3_lang2_gloss_id_col + ")) " +
				"ON e." + lang3_lang2_LI_col + " = te." + lang3_lang2_LI_col + " WHERE te." + lang3_lang2_LI_col + " IS NULL");
		
		// if not, include at least one chain in chain in triple table
		PreparedStatement insertStmt = dbConn.prepareStatement(
				"INSERT INTO `" + triplesPrefix + "_triple` " +
				"(id, " + lang1_lang2_DictPrefix + "_id, " + lang2_lang3_DictPrefix + "_id, " + lang3_lang2_DictPrefix + "_id, cluster1_id, triple_score, otic_score) " +
				"SELECT c.id, c." + lang1_lang2_DictPrefix + "_id, c." + lang2_lang3_DictPrefix + "_id, c." + lang3_lang2_DictPrefix + "_id, cluster1_id, chain_score, cluster1_score " +
				"FROM `" + triplesPrefix + "_temp` c INNER JOIN `" +
				lang3_lang2_dict_table + "` e ON (" + 
				lang3_lang2_DictPrefix + "_id = e." + lang3_lang2_gloss_id_col + ") " + 
				"WHERE " + lang3_lang2_LI_col + " = ? " +
				"ORDER BY cluster1_score desc LIMIT 1");
		
		while (missingTailLIs.next()) {
			insertStmt.setString(1, missingTailLIs.getString("LI"));
			insertStmt.executeUpdate();
		}
		
		insertStmt.close();
		missingTailLIs.close();		
	}

	private static void updateExistingSets () throws SQLException {
		ResultSet tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM TransEquiv");
		int teID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
		
		String mutual_lang1_dict_table;
		String mutual_lang1_LI_col;
//		String mutual_lang1_pos_col;
		String mutual_lang1_gloss_id_col;
		String mutual_lang1_DictPrefix;
		String mutual_lang2_dict_table;
		String mutual_lang2_LI_col;
//		String mutual_lang2_pos_col;
		String mutual_lang2_gloss_id_col;
		String mutual_lang2_DictPrefix;
		String new_lang_dict_table;
		String new_lang_LI_col;
		String new_lang_pos_col;
		String new_lang_gloss_id_col;
		String new_lang_DictPrefix;
		
		
		if (new_lang.equals(lang1)) {
			new_lang_dict_table = lang1_lang2_dict_table;
			new_lang_LI_col = lang1_lang2_LI_col;
			new_lang_pos_col = lang1_lang2_pos_col;
			new_lang_gloss_id_col = lang1_lang2_gloss_id_col;
			new_lang_DictPrefix = lang1_lang2_DictPrefix;
			
			mutual_lang1_dict_table = lang2_lang3_dict_table;
			mutual_lang1_LI_col = lang2_lang3_LI_col;
//			mutual_lang1_pos_col = lang2_lang3_pos_col;
			mutual_lang1_gloss_id_col = lang2_lang3_gloss_id_col;
			mutual_lang1_DictPrefix = lang2_lang3_DictPrefix;
			
			mutual_lang2_dict_table = lang3_lang2_dict_table;
			mutual_lang2_LI_col = lang3_lang2_LI_col;
//			mutual_lang2_pos_col = lang3_lang2_pos_col;
			mutual_lang2_gloss_id_col = lang3_lang2_gloss_id_col;
			mutual_lang2_DictPrefix = lang3_lang2_DictPrefix;
			
		} else if (new_lang.equals(lang2)) {
			new_lang_dict_table = lang2_lang3_dict_table;
			new_lang_LI_col = lang2_lang3_LI_col;
			new_lang_pos_col = lang2_lang3_pos_col;
			new_lang_gloss_id_col = lang2_lang3_gloss_id_col;
			new_lang_DictPrefix = lang2_lang3_DictPrefix;
			
			mutual_lang1_dict_table = lang1_lang2_dict_table;
			mutual_lang1_LI_col = lang1_lang2_LI_col;
//			mutual_lang1_pos_col = lang1_lang2_pos_col;
			mutual_lang1_gloss_id_col = lang1_lang2_gloss_id_col;
			mutual_lang1_DictPrefix = lang1_lang2_DictPrefix;

			mutual_lang2_dict_table = lang3_lang2_dict_table;
			mutual_lang2_LI_col = lang3_lang2_LI_col;
//			mutual_lang2_pos_col = lang3_lang2_pos_col;
			mutual_lang2_gloss_id_col = lang3_lang2_gloss_id_col;
			mutual_lang2_DictPrefix = lang3_lang2_DictPrefix;

		} else {
			new_lang_dict_table = lang3_lang2_dict_table;
			new_lang_LI_col = lang3_lang2_LI_col;
			new_lang_pos_col = lang3_lang2_pos_col;
			new_lang_gloss_id_col = lang3_lang2_gloss_id_col;
			new_lang_DictPrefix = lang3_lang2_DictPrefix;
			
			mutual_lang1_dict_table = lang1_lang2_dict_table;
			mutual_lang1_LI_col = lang1_lang2_LI_col;
//			mutual_lang1_pos_col = lang1_lang2_pos_col;
			mutual_lang1_gloss_id_col = lang1_lang2_gloss_id_col;
			mutual_lang1_DictPrefix = lang1_lang2_DictPrefix;

			mutual_lang2_dict_table = lang2_lang3_dict_table;
			mutual_lang2_LI_col = lang2_lang3_LI_col;
//			mutual_lang2_pos_col = lang2_lang3_pos_col;
			mutual_lang2_gloss_id_col = lang2_lang3_gloss_id_col;
			mutual_lang2_DictPrefix = lang2_lang3_DictPrefix;
		}


		
		// Search axis using mutual_lang1 and mutual_lang2
		String buffer = "SELECT DISTINCT axisID " +
				"FROM (TransEquiv TE_E INNER JOIN LexicalItem E ON (E.id = TE_E.liId) INNER JOIN " +
				"	`" + mutual_lang1_dict_table + "` EG " +
				"		ON (E.LI = EG." + mutual_lang1_LI_col + 
				"		AND EG." + mutual_lang1_gloss_id_col + " = ?) ) " +
				"INNER JOIN (TransEquiv TE_M INNER JOIN LexicalItem M ON (M.id = TE_M.liId) INNER JOIN " +
				"	`" + mutual_lang2_dict_table + "` MG " +
				"		ON M.LI = MG." + mutual_lang2_LI_col + 
				"		AND MG." + mutual_lang2_gloss_id_col + " = ?) " +
				"USING (axisID)";
//		System.out.println(buffer);
		PreparedStatement searchAxisStmt = dbConn.prepareStatement(buffer);
		
		// For looking up the id of the new language LI in LexicalItem table
		buffer = "SELECT DISTINCT LI.id FROM LexicalItem LI " +
				"INNER JOIN `" + new_lang_dict_table + "` hG " +
				"INNER JOIN " + new_lang_DictPrefix + "POS " +
						"ON ((" + new_lang_DictPrefix + "POS.stdPOS = LI.pos " +
								"AND hG.pos = " + new_lang_DictPrefix + "POS.pos) " +
						"OR hG." + new_lang_pos_col + " IS NULL) " +
				"AND LI.li = hG." + new_lang_LI_col + 
				" WHERE hG." + new_lang_gloss_id_col + " = ? AND lang = '" + new_lang + "'";
		PreparedStatement searchNewLangLIStmt = dbConn.prepareStatement(buffer);
		
		String addNewLangSQL = "INSERT IGNORE INTO TransEquiv " +
				"(id, axisId, liId, overallScore, oticScore, tripleScore) " +
				"values (?,?,?,?,?,?)";
		PreparedStatement addNewLangStmt = dbConn.prepareStatement(addNewLangSQL);
		
		int iter = 0;
		PreparedStatement getTripleCountStmt = dbConn.prepareStatement(
				"SELECT COUNT(*) FROM " + triplesPrefix + "_triple"); 
		String getRemainingTriplesSQL = 
				"SELECT DISTINCT id, " + mutual_lang1_DictPrefix + "_id m1ID, " +
				mutual_lang2_DictPrefix + "_id m2ID, " +
				new_lang_DictPrefix + "_id newID, triple_score, otic_score " +
				"FROM " + triplesPrefix + "_triple";
		PreparedStatement getTriplesStmt = dbConn.prepareStatement(
				getRemainingTriplesSQL);
		PreparedStatement deleteTripleStmt = dbConn.prepareStatement(
				"DELETE FROM " + triplesPrefix + "_triple " +
				"WHERE id = ?");
		ResultSet tRS = getTripleCountStmt.executeQuery();
		int startCount = 0;
		int endCount = tRS.next() ? tRS.getInt(1) : 0;
//		boolean foundAxis;
		do {
			System.out.println("Iteration " + ++iter);
			// how many triples left from previous iteration??
			startCount = endCount;
			System.out.println("Unused triples before: " + startCount);
			
			// for each triple
			ResultSet triples = getTriplesStmt.executeQuery();
			
			
			while (triples.next()) {
				// search if any Axis already contains mutual_lang1 LI and mutual_lang2 LI
				searchAxisStmt.setString(1, triples.getString("m1ID"));
				searchAxisStmt.setString(2, triples.getString("m2ID"));
				ResultSet axisRS = searchAxisStmt.executeQuery();
				while (axisRS.next()) { //IT EXISTS!!
					addNewLangStmt.setInt(2, axisRS.getInt("axisId"));
					// get new_lang LI's ID
					searchNewLangLIStmt.setString(1, triples.getString("newID"));
					ResultSet liRS = searchNewLangLIStmt.executeQuery();
					while (liRS.next()) {
						addNewLangStmt.setInt(1, ++teID);
						addNewLangStmt.setInt(3, liRS.getInt("ID"));
						addNewLangStmt.setDouble(4, 
								triples.getDouble("otic_score") * triples.getDouble("triple_score"));
						addNewLangStmt.setDouble(5, triples.getDouble("otic_score"));
						addNewLangStmt.setDouble(6, triples.getDouble("triple_score"));
						addNewLangStmt.executeUpdate();
					}
					deleteTripleStmt.setInt(1, triples.getInt("id"));
					deleteTripleStmt.executeUpdate();
				}
			}
			
			tRS = getTripleCountStmt.executeQuery();
			endCount = tRS.next() ? tRS.getInt(1) : 0;
			
			System.out.println("Unused triples after: " + endCount);
		} while (endCount < startCount);
		
	}

	private static void clusterSets() throws SQLException {
		int cluster2_id = 1;
		int cluster3_id = 1;

		// merge based on lang2-lang3
		PreparedStatement updateChainCluster2Stmt = dbConn.prepareStatement(
				"UPDATE (" + triplesPrefix +  "_triple a " +
				"  INNER JOIN " + triplesPrefix +  "_triple b USING (cluster1_id)) " +
				"  INNER JOIN " + triplesPrefix +  "_triple c ON (a.id = c.id) " +
				"SET c.cluster2_id = ? " +
				"WHERE b." + lang2_lang3_DictPrefix +  "_id = ?");
		// merge based on lang1-lang2
		PreparedStatement updateChainCluster3Stmt = dbConn.prepareStatement(
				"UPDATE (" + triplesPrefix +  "_triple a " +
				"  INNER JOIN " + triplesPrefix +  "_triple b USING (cluster2_id)) " +
				"  INNER JOIN " + triplesPrefix +  "_triple c ON (a.id = c.id) " +
				"SET c.cluster3_id = ? " +
				"WHERE b." + lang1_lang2_DictPrefix +  "_id = ?");
		

		stmt.execute(
				"UPDATE " + triplesPrefix +  "_triple " +
				"SET cluster2_id = NULL " +
				"WHERE cluster2_id IS NOT NULL");
		stmt.execute(
				"UPDATE " + triplesPrefix +  "_triple " +
				"SET cluster3_id = NULL " +
				"WHERE cluster3_id IS NOT NULL");
		
		ResultSet midLinksRS = stmt.executeQuery(
				"SELECT DISTINCT " + lang2_lang3_DictPrefix +  "_id FROM " + triplesPrefix +  "_triple");
		while (midLinksRS.next()) {
			updateChainCluster2Stmt.setInt(1, cluster2_id++);
			updateChainCluster2Stmt.setInt(2, midLinksRS.getInt(lang2_lang3_DictPrefix + "_id"));
			updateChainCluster2Stmt.executeUpdate();
		}
		
		updateChainCluster2Stmt.close();
		midLinksRS.close();
		
		ResultSet headLinksRS = stmt.executeQuery(
				"SELECT DISTINCT " + lang1_lang2_DictPrefix +  "_id FROM " + triplesPrefix +  "_triple");
		while (headLinksRS.next()) {
			updateChainCluster3Stmt.setInt(1, cluster3_id++);
			updateChainCluster3Stmt.setInt(2, headLinksRS.getInt(lang1_lang2_DictPrefix + "_id"));
			updateChainCluster3Stmt.executeUpdate();
		}
		
		updateChainCluster3Stmt.close();
		headLinksRS.close();
	}


	private static void importAxis() throws SQLException {
			ResultSet tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM Axis");
			int curAxisID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
			tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM TransEquiv");
			int curTEID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
			tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM TransGlossEquiv");
			int curTGEID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
			
			PreparedStatement addAxisStmt = dbConn.prepareStatement(
					"INSERT INTO Axis (id) VALUES (?)");
			PreparedStatement addTEStmt = dbConn.prepareStatement(
					"INSERT INTO TransEquiv (id, axisId, liId, overallScore, oticScore, tripleScore) " +
					"VALUES (?, ?, ?, ?, ?, ?)");
			PreparedStatement addTEGlossStmt = dbConn.prepareStatement(
					"INSERT INTO TransGlossEquiv (id, axisId, glossId) VALUES (?, ?, ?)");
			
			String listLang1Lang2LISQL = "SELECT LexicalItem.id liId, LexicalItem.LI, "
					+ "AVG(otic_score * triple_score) a, AVG(otic_score) b, AVG(triple_score) c "
					+ "FROM " + triplesPrefix + "_triple INNER JOIN `" + lang1_lang2_dict_table + "` g "
					+ "ON " + lang1_lang2_DictPrefix + "_id = " + lang1_lang2_gloss_id_col + " "
					+ "LEFT JOIN " + lang1_lang2_DictPrefix + "POS ON (g.pos = " + lang1_lang2_DictPrefix + "POS.pos) "
					+ "LEFT JOIN LexicalItem ON (g." + lang1_lang2_LI_col + " = LexicalItem.li " 
					+ "AND (" + lang1_lang2_DictPrefix + "POS.stdPOS = LexicalItem.pos OR LexicalItem.pos IS NULL)) "
					+ "WHERE cluster3_id = ? AND lang = '" + lang1 + "' "
					+ "GROUP BY LexicalItem.id";
			PreparedStatement listLang1Lang2LIStmt = dbConn.prepareStatement(listLang1Lang2LISQL);
			String listLang1Lang2GlossSQL = "SELECT DISTINCT Gloss.id " +
					"FROM Gloss INNER JOIN LexicalItem ON (liId = LexicalItem.id) " +
					"INNER JOIN " + triplesPrefix + "_triple ON (srcdict = CONCAT('" + lang1_lang2_DictPrefix + "#', " + lang1_lang2_DictPrefix + "_id)) " +
					"WHERE liId = ? AND cluster3_id = ?";
			PreparedStatement listLang1Lang2GlossStmt = dbConn.prepareStatement(listLang1Lang2GlossSQL);
			
			String listLang2Lang3LISQL = "SELECT LexicalItem.id liId, LexicalItem.LI, "
					+ "AVG(otic_score * triple_score) a, AVG(otic_score) b, AVG(triple_score) c "
					+ "FROM " + triplesPrefix + "_triple INNER JOIN `" + lang2_lang3_dict_table + "` g "
					+ "ON " + lang2_lang3_DictPrefix + "_id = " + lang2_lang3_gloss_id_col + " "
					+ "LEFT JOIN " + lang2_lang3_DictPrefix + "POS ON (g.pos = " + lang2_lang3_DictPrefix + "POS.pos) "
					+ "LEFT JOIN LexicalItem ON (g." + lang2_lang3_LI_col + " = LexicalItem.li " 
					+ "AND (" + lang2_lang3_DictPrefix + "POS.stdPOS = LexicalItem.pos OR LexicalItem.pos IS NULL)) "
					+ "WHERE cluster3_id = ? AND lang = '" + lang2 + "' "
					+ "GROUP BY LexicalItem.id";
			PreparedStatement listLang2Lang3LIStmt = dbConn.prepareStatement(listLang2Lang3LISQL);
			String listLang2Lang3GlossSQL = "SELECT DISTINCT Gloss.id " +
					"FROM Gloss INNER JOIN LexicalItem ON (liId = LexicalItem.id) " +
					"INNER JOIN " + triplesPrefix + "_triple ON (srcdict = CONCAT('" + lang2_lang3_DictPrefix + "#', " + lang2_lang3_DictPrefix + "_id)) " +
					"WHERE liId = ? AND cluster3_id = ?";
			PreparedStatement listLang2Lang3GlossStmt = dbConn.prepareStatement(listLang2Lang3GlossSQL);
			
			String listLang3Lang2LISQL = "SELECT LexicalItem.id liId, LexicalItem.LI, "
					+ "AVG(otic_score * triple_score) a, AVG(otic_score) b, AVG(triple_score) c "
					+ "FROM " + triplesPrefix + "_triple INNER JOIN `" + lang3_lang2_dict_table + "` g "
					+ "ON " + lang3_lang2_DictPrefix + "_id = " + lang3_lang2_gloss_id_col + " "
					+ "LEFT JOIN " + lang3_lang2_DictPrefix + "POS ON (g.pos = " + lang3_lang2_DictPrefix + "POS.pos) "
					+ "LEFT JOIN LexicalItem ON (g." + lang3_lang2_LI_col + " = LexicalItem.li " 
					+ "AND (" + lang3_lang2_DictPrefix + "POS.stdPOS = LexicalItem.pos OR LexicalItem.pos IS NULL)) "
					+ "WHERE cluster3_id = ? AND lang = '" + lang3 + "' "
					+ "GROUP BY LexicalItem.id";
			PreparedStatement listLang3Lang2LIStmt = dbConn.prepareStatement(listLang3Lang2LISQL);
			String listLang3Lang2GlossSQL = "SELECT DISTINCT Gloss.id " +
					"FROM Gloss INNER JOIN LexicalItem ON (liId = LexicalItem.id) " +
					"INNER JOIN " + triplesPrefix + "_triple ON (srcdict = CONCAT('" + lang3_lang2_DictPrefix + "#', " + lang3_lang2_DictPrefix + "_id)) " +
					"WHERE liId = ? AND cluster3_id = ?";
			PreparedStatement listLang3Lang2GlossStmt = dbConn.prepareStatement(listLang3Lang2GlossSQL);
			
			
			
			//list all translation sets, list all members, insert into TransEquiv
			String listSetsSQL = "SELECT DISTINCT cluster3_id FROM " + triplesPrefix + "_triple";
			ResultSet clusters = stmt.executeQuery(listSetsSQL);
			int c3Id;
			ResultSet LIs, glosses;
			while (clusters.next()) {
				// assign a new axis 
				c3Id = clusters.getInt("cluster3_id");
				addAxisStmt.setInt(1, ++curAxisID);
	//			addAxisStmt.setInt(2, c3Id);
				addAxisStmt.executeUpdate();
				
				addTEStmt.setInt(2, curAxisID);
				addTEGlossStmt.setInt(2, curAxisID);
				
				// List LIs, Add to TE
				listLang1Lang2LIStmt.setInt(1, c3Id);
				listLang1Lang2GlossStmt.setInt(2, c3Id);
				LIs = listLang1Lang2LIStmt.executeQuery();
				while (LIs.next()) {
					addTEStmt.setInt(1, ++curTEID);
					addTEStmt.setInt(3, LIs.getInt("liId"));
					addTEStmt.setDouble(4, LIs.getDouble("a"));
					addTEStmt.setDouble(5, LIs.getDouble("b"));
					addTEStmt.setDouble(6, LIs.getDouble("c"));
					addTEStmt.executeUpdate();
					
					listLang1Lang2GlossStmt.setInt(1, LIs.getInt("liId"));
					glosses = listLang1Lang2GlossStmt.executeQuery();
					while (glosses.next()) {
						addTEGlossStmt.setInt(1, ++curTGEID);
						addTEGlossStmt.setInt(3, glosses.getInt("id"));
						addTEGlossStmt.executeUpdate();
					}
	
				}
	
				listLang2Lang3LIStmt.setInt(1, c3Id);
				listLang2Lang3GlossStmt.setInt(2, c3Id);
				LIs = listLang2Lang3LIStmt.executeQuery();
				while (LIs.next()) {
					addTEStmt.setInt(1, ++curTEID);
					addTEStmt.setInt(3, LIs.getInt("liId"));
					addTEStmt.setDouble(4, LIs.getDouble("a"));
					addTEStmt.setDouble(5, LIs.getDouble("b"));
					addTEStmt.setDouble(6, LIs.getDouble("c"));
					addTEStmt.executeUpdate();
					
					listLang2Lang3GlossStmt.setInt(1, LIs.getInt("liId"));
					glosses = listLang2Lang3GlossStmt.executeQuery();
					while (glosses.next()) {
						addTEGlossStmt.setInt(1, ++curTGEID);
						addTEGlossStmt.setInt(3, glosses.getInt("id"));
						addTEGlossStmt.executeUpdate();
					}
				}
	
				listLang3Lang2LIStmt.setInt(1, c3Id);
				listLang3Lang2GlossStmt.setInt(2, c3Id);
				LIs = listLang3Lang2LIStmt.executeQuery();
				while (LIs.next()) {
					addTEStmt.setInt(1, ++curTEID);
					addTEStmt.setInt(3, LIs.getInt("liId"));
					addTEStmt.setDouble(4, LIs.getDouble("a"));
					addTEStmt.setDouble(5, LIs.getDouble("b"));
					addTEStmt.setDouble(6, LIs.getDouble("c"));
					addTEStmt.executeUpdate();
					
					listLang3Lang2GlossStmt.setInt(1, LIs.getInt("liId"));
					glosses = listLang3Lang2GlossStmt.executeQuery();
					while (glosses.next()) {
						addTEGlossStmt.setInt(1, ++curTGEID);
						addTEGlossStmt.setInt(3, glosses.getInt("id"));
						addTEGlossStmt.executeUpdate();
					}
	
				}
				
			}
			
			listLang1Lang2GlossStmt.close();
			listLang1Lang2LIStmt.close();
			listLang2Lang3GlossStmt.close();
			listLang2Lang3LIStmt.close();
			listLang3Lang2GlossStmt.close();
			listLang3Lang2LIStmt.close();
			addTEGlossStmt.close();
			addTEStmt.close();
			clusters.close();
			addAxisStmt.close();
		}


	private static void shutdown() throws SQLException {
		stmt.close();
		dbConn.close();
//		logger.close();		
	}
	
//	private static void log(String s) {
//		if (debug) {
////			System.out.println(s);
//			logger.println(s);
//		}
//	}
	
//	//TODO
//	private static void addIban() throws SQLException {
//			// search for overlaps of English-Malay pairs from hkp_triple and existing multiling.
//			// for each hkp_triple, see if there's any Axis that already contains the English and 
//			// Malay LIs. If yes, add Iban LI to that Axis. If no, create new Axis.
//			Map<String, List<ScoreTriple>> scoreList = new HashMap<String, List<ScoreTriple>>();
//			
//			int transEquivId = 64022;
//			int newAxisId = 12218;
//			stmt.execute("DELETE FROM TransEquiv WHERE id > " + transEquivId);
//			stmt.execute("DELETE FROM Axis WHERE id > " + newAxisId);
//			String searchAxisSQL = "SELECT axisID " +
//					"FROM (TransEquiv TE_E INNER JOIN LexicalItem E ON (E.id = TE_E.liId) INNER JOIN " +
//					"	(`kimd-li` EL INNER JOIN `kimd-gloss-ms` EG ON EG.li_id = EL.id AND EG.id = ?) " +
//					"		ON (E.LI = EL.LI) ) " +
//					"INNER JOIN (TransEquiv TE_M INNER JOIN LexicalItem M ON (M.id = TE_M.liId) INNER JOIN " +
//					"	`ptsdict-gloss-ms` MG ON M.LI = MG.gloss AND MG.id = ?) " +
//					"USING (axisID)";
//			PreparedStatement searchAxisStmt = dbConn.prepareStatement(searchAxisSQL);
//			
//			String searchIbaSQL = "SELECT DISTINCT LI.id FROM LexicalItem LI " +
//					"INNER JOIN hrie_LI hLI ON LI.li = hLI.li " +
//					"INNER JOIN hrie_gloss_en hG ON (hg.li_id = hLI.id) " +
//					"INNER JOIN hriePOS ON ((hriePOS.stdPOS = LI.pos AND hG.pos = hriePOS.pos) " +
//					"OR hG.pos IS NULL) " +
//					"WHERE hG.id = ? AND lang = 'iba'";
//			PreparedStatement searchIbaStmt = dbConn.prepareStatement(searchIbaSQL);
//			
//			String searchEngSQL = "SELECT DISTINCT LI.id FROM LexicalItem LI " +
//					"INNER JOIN `kimd-LI` hLI ON LI.li = hLI.li " +
//					"INNER JOIN `kimd-gloss-ms` hG ON (hg.li_id = hLI.id) " +
//					"INNER JOIN kimdPOS ON ((kimdPOS.stdPOS = LI.pos AND hLI.pos = kimdPOS.pos) OR hLI.pos IS NULL) " +
//					"WHERE hG.id = ? AND lang = 'eng'";
//			PreparedStatement searchEngStmt = dbConn.prepareStatement(searchEngSQL);
//			
//			String searchMsaSQL = "SELECT DISTINCT LI.id FROM LexicalItem LI " +
//					"INNER JOIN `ptsdict-gloss-ms` hG ON (LI.li = hG.gloss) " +
//					"LEFT JOIN ptsPOS ON " +
//					"  ((ptsPOS.stdPOS RLIKE '[[:<:]]' || LI.pos || '[[:>:]]') AND hG.POS = ptsPOS.pos) " +
//					"  OR hG.POS IS NULL " +
//					"WHERE hG.id = ? AND lang = 'msa'";
//			PreparedStatement searchMsaStmt = dbConn.prepareStatement(searchMsaSQL);
//			
//			String addIbanSQL = "INSERT INTO TransEquiv " +
//					"(id, axisId, liId, overallScore, oticScore, tripleScore) " +
//					"values (?,?,?,?,?,?)";
//			PreparedStatement addEquivStmt = dbConn.prepareStatement(addIbanSQL);
//			
//			PreparedStatement addAxisStmt = dbConn.prepareStatement("INSERT INTO Axis (id) VALUES (?)");
//					
//			// for each hkp triple
//			ResultSet triples = stmt.executeQuery(
//					"SELECT hrie_id, kimd_id, pts_id, triple_score, otic_score FROM hkp_triple");
//			
//			
//			while (triples.next()) {
//				searchIbaStmt.setInt(1, triples.getInt("hrie_id"));
//				ResultSet ibaLI = searchIbaStmt.executeQuery();
//				ibaLI.next();
//				int ibaLIID = ibaLI.getInt("id");
//				ibaLI.close();
//	
//				// find if there is an existing axis with this english and malay
//				boolean foundAxis = false;
//				searchAxisStmt.setInt(1, triples.getInt("kimd_id"));
//				searchAxisStmt.setInt(2, triples.getInt("pts_id"));
//				// if yes 
//				ResultSet axes = searchAxisStmt.executeQuery();
//				while (axes.next()) {
//					foundAxis = true;
//					int axisID = axes.getInt("axisID");
//					String key = axisID + "-" + ibaLIID;
//					// put ibaLI + axis in list with scores
//					if (scoreList.containsKey(key)) {
//						scoreList.get(key).add(new ScoreTriple(
//								triples.getDouble("triple_score"), triples.getDouble("otic_score")));
//					} else {
//						List<ScoreTriple> newList = new ArrayList<ScoreTriple>();
//						newList.add(new ScoreTriple(
//								triples.getDouble("triple_score"), triples.getDouble("otic_score")));
//						scoreList.put(key, newList);
//					}
//				}
//				// if no, create new axis
//				if (!foundAxis) {
//					// TODO
//					addAxisStmt.setInt(1, ++newAxisId);
//					addAxisStmt.executeUpdate();
//					
//					addEquivStmt.setInt(1, ++transEquivId);
//					addEquivStmt.setInt(2, newAxisId);
//					addEquivStmt.setInt(3, ibaLIID);
//					addEquivStmt.setDouble(4, triples.getDouble("triple_score") * triples.getDouble("otic_score"));
//					addEquivStmt.setDouble(5, triples.getDouble("otic_score"));
//					addEquivStmt.setDouble(6, triples.getDouble("triple_score"));
//					addEquivStmt.executeUpdate();
//					
//					searchEngStmt.setInt(1, triples.getInt("kimd_id"));
//					ResultSet rs = searchEngStmt.executeQuery();
//					if (rs.next()) {
//						addEquivStmt.setInt(1, ++transEquivId);
//						addEquivStmt.setInt(2, newAxisId);
//						addEquivStmt.setInt(3, rs.getInt("id"));
//						addEquivStmt.setDouble(4, triples.getDouble("triple_score") * triples.getDouble("otic_score"));
//						addEquivStmt.setDouble(5, triples.getDouble("otic_score"));
//						addEquivStmt.setDouble(6, triples.getDouble("triple_score"));
//						addEquivStmt.executeUpdate();
//					}
//					
//					searchMsaStmt.setInt(1, triples.getInt("pts_id"));
//					rs = searchMsaStmt.executeQuery();
//					if (rs.next()) {
//						addEquivStmt.setInt(1, ++transEquivId);
//						addEquivStmt.setInt(2, newAxisId);
//						addEquivStmt.setInt(3, rs.getInt("id"));
//						addEquivStmt.setDouble(4, triples.getDouble("triple_score") * triples.getDouble("otic_score"));
//						addEquivStmt.setDouble(5, triples.getDouble("otic_score"));
//						addEquivStmt.setDouble(6, triples.getDouble("triple_score"));
//						addEquivStmt.executeUpdate();
//					} else {
//						System.err.println("I didn't found pts#" + triples.getInt("pts_id"));
//	//					System.exit(1);
//						continue;
//					}
//	
//	
//	//				String key = newAxisId + "-" + ibaLIID;
//	//				// put ibaLI + axis in list with scores
//	//				if (scoreList.containsKey(key)) {
//	//					scoreList.get(key).add(new ScoreTriple(
//	//							triples.getDouble("triple_score"), triples.getDouble("otic_score")));
//	//				} else {
//	//					List<ScoreTriple> newList = new ArrayList<ScoreTriple>();
//	//					newList.add(new ScoreTriple(
//	//							triples.getDouble("triple_score"), triples.getDouble("otic_score")));
//	//					scoreList.put(key, newList);
//	//				}
//				}
//			}
//			
//			triples.close();
//			
//			for (String k : scoreList.keySet()) {
//				String[] toks = k.split("-");
//				int axisID = Integer.parseInt(toks[0]);
//				int liID = Integer.parseInt(toks[1]);
//				List<ScoreTriple> l = scoreList.get(k);
//				double sumOTICScore = 0d, sumTripleScore = 0d, sumOverallScore = 0d;
//				for (ScoreTriple st : l) {
//					sumOTICScore += st.oticScore;
//					sumTripleScore += st.tripleScore;
//					sumOverallScore += st.overallScore;
//				}
//				
//				addEquivStmt.setInt(1, ++transEquivId);
//				addEquivStmt.setInt(2, axisID);
//				addEquivStmt.setInt(3, liID);
//				addEquivStmt.setDouble(4, sumOverallScore / l.size());
//				addEquivStmt.setDouble(5, sumOTICScore / l.size());
//				addEquivStmt.setDouble(6, sumTripleScore / l.size());
//				addEquivStmt.executeUpdate();
//			}
//		}
//
//
//
//	public static class ScoreTriple {
//		double tripleScore;
//		double oticScore;
//		double overallScore;
//		
//		public ScoreTriple(double tScore, double oScore) {
//			this.tripleScore = tScore;
//			this.oticScore = oScore;
//			this.overallScore = tScore * oScore;
//		}
//	}
}
