package liantze.build;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

public class ImportFeM extends LexiconTXTool {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		ResourceBundle bundle = ResourceBundle.getBundle("props/fra-eng-msa");
		initParams(bundle);
		connectDB();
		
		ResultSet tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM TransEquiv");
		int teID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
		tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM Axis");
		int axisID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
		tmpRS = stmt.executeQuery("SELECT max(id) max_id FROM LexicalItem");
		int liID = tmpRS.next() ? tmpRS.getInt("max_id") : 0;
		
		String buffer;
		
		// create fem_triple manually
		buffer = "create table fem_triple ( " +
				"id int primary key, " +
				"frWord varchar(100), enWord varchar(100), msWord varchar(100), stdPOS varchar(10), " +
				"cluster1_id int, cluster2_id int, cluster3_id int, " +
				"constraint unique (frWord, enWord, msWord, stdpos), " +
				"index (frWord), index (enWord), index (msWord), index (stdPOS), " +
				"index (cluster1_id), index (cluster2_id), index (cluster3_id) " +
				") engine = innodb";
		stmt.execute("DROP TABLE IF EXISTS fem_triple");
		stmt.execute(buffer);
		
		// Search axis using ms and en
		buffer = "SELECT DISTINCT TE_E.axisID axisID " +
				"FROM (TransEquiv TE_E INNER JOIN LexicalItem E ON (E.id = TE_E.liId) " +
				"	INNER JOIN `xdict-en-zh` EL ON (EL.LI = E.LI AND E.LI = ?)) " +
				"INNER JOIN (TransEquiv TE_M INNER JOIN LexicalItem M ON (M.id = TE_M.liId) " +
				"	INNER JOIN `sistecem-en-ms` ML ON (ML.gloss = M.LI AND M.LI = ?)) " +
				"USING (axisID)";
		PreparedStatement searchAxisStmt = dbConn.prepareStatement(buffer);
		
		// Get triples
		buffer = "SELECT frWord, enWord, msWord, stdPOS FROM fem_triple WHERE cluster3_id = ?";
		PreparedStatement getTripleStmt = dbConn.prepareStatement(buffer);
		
		// Search LexicalItem id of en and ms
		buffer = "SELECT DISTINCT id FROM LexicalItem " +
				"WHERE LI = ? AND lang = ? AND pos = ?";
		PreparedStatement searchLIStmt = dbConn.prepareStatement(buffer);
		
		buffer = "INSERT INTO LexicalItem (id, lang, li, pos) VALUES (?,?,?,?)";
		PreparedStatement addLIStmt = dbConn.prepareStatement(buffer);
		
		buffer = "INSERT INTO Axis (id) VALUES (?)";
		PreparedStatement addAxisStmt = dbConn.prepareStatement(buffer);
		
		// insert new transequiv
		buffer = "INSERT IGNORE INTO TransEquiv (id, axisID, liID, overallScore, oticScore, tripleScore) " +
				"VALUES (?, ?, ?, 1, 1, 1)";
		PreparedStatement addTransEquivStmt = dbConn.prepareStatement(buffer);
		
		// Get all FeM triples
		buffer = "SELECT frId, frWord, stdPos, " +
				"enId, polished_enWord, msId, polished_msWord " +
				"FROM `fem-fr-en-ms` INNER JOIN femPOS ON frPOS = pos " +
				"WHERE polished_enWord IS NOT NULL AND polished_msWord IS NOT NULL";
		ResultSet FeMRS = stmt.executeQuery(buffer);
		boolean foundAxis;
		
		Set<String[]> unusedTriples = new HashSet<String[]> ();
		
		while (FeMRS.next()) {
			foundAxis = false;
			// search if any Axis already contains polished_enWord and polished_msWord
			searchAxisStmt.setString(1, FeMRS.getString("polished_enWord"));
			searchAxisStmt.setString(2, FeMRS.getString("polished_msWord"));
			ResultSet axisRS = searchAxisStmt.executeQuery();
			while (axisRS.next()) { //真的有！
				foundAxis = true;
				addTransEquivStmt.setInt(2, axisRS.getInt("axisId"));
				// 快找找 frWord 的 ID!
				searchLIStmt.setString(1, FeMRS.getString("frWord"));
				searchLIStmt.setString(2, "fra");
				searchLIStmt.setString(3, FeMRS.getString("stdPOS"));
				ResultSet liRS = searchLIStmt.executeQuery();
				while (liRS.next()) {
					addTransEquivStmt.setInt(1, ++teID);
					addTransEquivStmt.setInt(3, liRS.getInt("ID"));
					addTransEquivStmt.executeUpdate();
				}
			}

			if (!foundAxis) {
				// 如果没有找到 axis 呢，留下来用
				unusedTriples.add( new String[] {
						FeMRS.getString("frWord"),
						FeMRS.getString("polished_enWord"),
						FeMRS.getString("polished_msWord"),
						FeMRS.getString("stdPOS")
				});
			}
		}
		
		int unusedCount = unusedTriples.size();
		int iter = 0;
		do {
			System.out.println("Iteration " + ++iter);
			Set<String[]> triplesCopy = new HashSet<String[]> ();
			unusedCount = unusedTriples.size();
			triplesCopy.addAll(unusedTriples);
			for (String[] triple : triplesCopy) {
				foundAxis = false;
				// search if any Axis already contains polished_enWord and polished_msWord
				searchAxisStmt.setString(1, triple[1]);
				searchAxisStmt.setString(2, triple[2]);
				ResultSet axisRS = searchAxisStmt.executeQuery();
				while (axisRS.next()) { //真的有！
					foundAxis = true;
					addTransEquivStmt.setInt(2, axisRS.getInt("axisId"));
					// 快找找 frWord 的 ID!
					searchLIStmt.setString(1, triple[0]);
					searchLIStmt.setString(2, "fra");
					searchLIStmt.setString(3, triple[3]);
					ResultSet liRS = searchLIStmt.executeQuery();
					while (liRS.next()) {
						addTransEquivStmt.setInt(1, ++teID);
						addTransEquivStmt.setInt(3, liRS.getInt("ID"));
						addTransEquivStmt.executeUpdate();
					}
					unusedTriples.remove(triple);
				}
			}
			System.out.println("Unused triples before: " + unusedCount);
			System.out.println("Unused triples after: " + unusedTriples.size());
		} while (unusedTriples.size() < unusedCount);
		
		System.out.println("Stopped. Clustering unused triples now.");

		
		Map<String, Set<String[]>> firstRound = new HashMap<String, Set<String[]>> ();
		for (String[] triple : unusedTriples) {
			String key = triple[0] + "--" + triple[1];
			if (!firstRound.containsKey(key)) {
				firstRound.put(key, new HashSet<String[]> ());
			} 
			firstRound.get(key).add(triple);
		}
		
		PreparedStatement addTripleStmt = dbConn.prepareStatement(
				"INSERT IGNORE INTO fem_triple (id, frWord, enWord, msWord, stdPos, cluster1_id) VALUES (?,?,?,?,?,?)");
		int id = 0;
		int cluster1_id = 0;
		int cluster2_id = 0;
		int cluster3_id = 0;
		for (Set<String[]> set : firstRound.values()) {
			addTripleStmt.setInt(6, ++cluster1_id);
			for (String[] triple : set) {
				addTripleStmt.setInt(1, ++id);
				addTripleStmt.setString(2, triple[0]);
				addTripleStmt.setString(3, triple[1]);
				addTripleStmt.setString(4, triple[2]);
				addTripleStmt.setString(5, triple[3]);
				addTripleStmt.executeUpdate();
			}
		}
		
		PreparedStatement updateChainCluster2Stmt = dbConn.prepareStatement(
				"UPDATE (fem_triple a " +
				"  INNER JOIN fem_triple b USING (cluster1_id)) " +
				"  INNER JOIN fem_triple c ON (a.id = c.id) " +
				"SET c.cluster2_id = ? " +
				"WHERE b.frWord = ? AND b.msWord = ?");

		PreparedStatement updateChainCluster3Stmt = dbConn.prepareStatement(
				"UPDATE (fem_triple a " +
				"  INNER JOIN fem_triple b USING (cluster2_id)) " +
				"  INNER JOIN fem_triple c ON (a.id = c.id) " +
				"SET c.cluster3_id = ? " +
				"WHERE b.enWord = ? AND b.msWord = ?");

		System.out.println("Retriving fra-msa");
		tmpRS = stmt.executeQuery("SELECT DISTINCT frWord, msWord FROM fem_triple");
		while (tmpRS.next()) {
			updateChainCluster2Stmt.setInt(1, ++cluster2_id);
			updateChainCluster2Stmt.setString(2, tmpRS.getString("frWord"));
			updateChainCluster2Stmt.setString(3, tmpRS.getString("msWord"));
			updateChainCluster2Stmt.executeUpdate();
		}
		
		System.out.println("Retriving eng-msa");
		tmpRS = stmt.executeQuery("SELECT DISTINCT enWord, msWord FROM fem_triple");
		while (tmpRS.next()) {
			updateChainCluster3Stmt.setInt(1, ++cluster3_id);
			updateChainCluster3Stmt.setString(2, tmpRS.getString("enWord"));
			updateChainCluster3Stmt.setString(3, tmpRS.getString("msWord"));
			updateChainCluster3Stmt.executeUpdate();
		}

		// TODO import Axis
		// Go thru each cluster3_id
		ResultSet c3 = stmt.executeQuery("SELECT DISTINCT cluster3_id FROM fem_triple");
		while (c3.next()) {
			// Create a new axis
			addAxisStmt.setInt(1, ++axisID);
			addAxisStmt.executeUpdate();
			
			addTransEquivStmt.setInt(2, axisID);
			
			// Go thru each fem_triple
			getTripleStmt.setInt(1, c3.getInt("cluster3_id"));
			ResultSet tripleRS = getTripleStmt.executeQuery();
			int foundLIid; 
			while (tripleRS.next()) {
				String frWord = tripleRS.getString("frWord");
				String enWord = tripleRS.getString("enWord");
				String msWord = tripleRS.getString("msWord");
				String pos = tripleRS.getString("stdpos");
				
				// search id for frWord, enWord and msWord
				searchLIStmt.setString(3, pos);
				
				// if they don't exist, create a new lexicalitem
				// add new transequiv for frWord, enWord, msWord
				searchLIStmt.setString(1, frWord);
				searchLIStmt.setString(2, "fra");
				tmpRS = searchLIStmt.executeQuery();
				if (tmpRS.next()) {
					foundLIid = tmpRS.getInt("id");
				} else {
					addLIStmt.setInt(1, ++liID);
					addLIStmt.setString(2, "fra");
					addLIStmt.setString(3, frWord);
					addLIStmt.setString(4, pos);
					addLIStmt.executeUpdate();
					foundLIid = liID;
				}				
				addTransEquivStmt.setInt(1, ++teID);
				addTransEquivStmt.setInt(3, foundLIid);
				addTransEquivStmt.executeUpdate();
				
				searchLIStmt.setString(1, enWord);
				searchLIStmt.setString(2, "eng");
				tmpRS = searchLIStmt.executeQuery();
				if (tmpRS.next()) {
					foundLIid = tmpRS.getInt("id");
				} else {
					addLIStmt.setInt(1, ++liID);
					addLIStmt.setString(2, "eng");
					addLIStmt.setString(3, enWord);
					addLIStmt.setString(4, pos);
					addLIStmt.executeUpdate();
					foundLIid = liID;
				}				
				addTransEquivStmt.setInt(1, ++teID);
				addTransEquivStmt.setInt(3, foundLIid);
				addTransEquivStmt.executeUpdate();
				
				searchLIStmt.setString(1, msWord);
				searchLIStmt.setString(2, "msa");
				tmpRS = searchLIStmt.executeQuery();
				if (tmpRS.next()) {
					foundLIid = tmpRS.getInt("id");
				} else {
					addLIStmt.setInt(1, ++liID);
					addLIStmt.setString(2, "msa");
					addLIStmt.setString(3, msWord);
					addLIStmt.setString(4, pos);
					addLIStmt.executeUpdate();
					foundLIid = liID;
				}				
				addTransEquivStmt.setInt(1, ++teID);
				addTransEquivStmt.setInt(3, foundLIid);
				addTransEquivStmt.executeUpdate();
				
			}
		}
	}

}
