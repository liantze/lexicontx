/* @(#)BilingDictExtractor.java      1.0   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.build;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import liantze.util.FileUtils;

/**
 * <p>Extracts a bilingual dictionary form an existing multilingual dictionary,
 * suitable for use with SiSTeC-ebmt. Note that the output dictionary currently 
 * makes NO sense distinctions among the translation mappings.</p>
 * 
 * <p>Invocation:</p>
 * <code>java liantze.build.BilingDictExtractor properties-file src-lang tgt-lang output-file</code>
 * 
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public class BilingDictExtractor extends LexiconTXTool {
	
	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		if (args.length < 4) {
			System.err.println("Usage: java BilingDictExtractor <task-props> <srcLang> <tgtLang> <output-file>");
			System.exit(1);
		}
		ResourceBundle bundle = ResourceBundle.getBundle(args[0]);
		PrintWriter writer = FileUtils.getUTF8Writer(args[3]);
		initParams(bundle);
		connectDB();
		
		extractDict(args[1], args[2], writer);
		
		writer.close();
		disconnectDB();
		
	}
	
	private static void extractDict(String srcLang, String tgtLang, PrintWriter writer) throws SQLException {
		String findLITrans = 	
				 "SELECT DISTINCT S.LI srcWord, S.pos srcPOS, " +
//				 "GROUP_CONCAT(DISTINCT T.LI SEPARATOR ', ') translation " +
				 "T.LI translation " +
				 "FROM LexicalItem S INNER JOIN TransEquiv ST ON (S.id = ST.liid) " +
				 "INNER JOIN Axis SA ON ST.axisID = SA.id " +
				 "INNER JOIN Axis TA ON TA.id = SA.id  " +
				 "INNER JOIN TransEquiv TT ON (TT.axisID = TA.id) " +
				 "INNER JOIN LexicalItem T ON (T.id = TT.liid) " +
				 "WHERE S.lang = ? AND T.lang = ? " ; //+
//				 "GROUP BY srcWord, srcPOS";
		String findGlossTrans =
				 "SELECT DISTINCT S.LI srcWord, S.pos srcPOS, " +
//				 "GROUP_CONCAT(DISTINCT T.glossStr SEPARATOR ', ') translation " +
				 "T.glossStr translation " +
				 "FROM LexicalItem S INNER JOIN TransEquiv ST ON (S.id = ST.liid) " +
				 "INNER JOIN Axis SA ON ST.axisID = SA.id " +
				 "INNER JOIN Axis TA ON TA.id = SA.id  " +
				 "INNER JOIN TransGlossEquiv TT ON (TT.axisID = TA.id) " +
				 "INNER JOIN Gloss T ON (T.id = TT.glossid) " +
				 "WHERE S.lang = ? AND T.lang = ? " ; //+
//				 "GROUP BY srcWord, srcPOS";
		PreparedStatement extractStmt = dbConn.prepareStatement(
				"(" + findLITrans + ") UNION (" + findGlossTrans + ") ORDER BY srcWord, srcPOS");
		
		extractStmt.setString(1, srcLang);
		extractStmt.setString(2, tgtLang);
		extractStmt.setString(3, srcLang);
		extractStmt.setString(4, tgtLang);

		ResultSet newDict = extractStmt.executeQuery();
		while (newDict.next()) {
			writer.print(newDict.getString("srcWord"));
			writer.print("\t");
			writer.print(newDict.getString("srcPOS"));
			writer.print("\t");
			writer.print(newDict.getString("translation"));
			writer.println();
		}
	}

}
