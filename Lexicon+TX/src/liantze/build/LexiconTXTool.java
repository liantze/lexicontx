/* @(#)LexiconTXTool.java      1.0   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.build;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import liantze.struct.DictSource;
import liantze.util.DBUtils;

/**
 * Just an abstract class where some common properties are initialised
 * for the benefit of the other Lexicon+TX tools. This is probably bad
 * software engineering practice, I dunno. <code>>_<</code>
 * 
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public abstract class LexiconTXTool {
	
	static PrintWriter logger;

	static Connection dbConn;	
	static Statement stmt;

	/* Languages and input dictionaries. */
	static String lang1;
	static String lang2;
	static String lang3;
	static DictSource lang1_lang2_Dict;
	static DictSource lang2_lang3_Dict;
	static DictSource lang3_lang2_Dict;
	static String lang1_lang2_DictPrefix;
	static String lang2_lang3_DictPrefix;
	static String lang3_lang2_DictPrefix;
	
	/* triples table name and log file name */
	static String triplesPrefix;
	static String logPrefix;
	
	/* database connection details */
	static String dbHost;
	static int dbPort;
	static String dbName;
	static String dbUsername;
	static String dbPassword;
	
	/* 
	 * Set the queries so that you can get unique rows of 
	 * - the Lang1 LIs,
	 * - for each biling dict: unique rows 
	 *   (gloss_id, source language LI, target language gloss, POS)
	 *   where gloss_id is a unique numerical identifier for mappings of LI -> (gloss, POS).
	 * May be easier to either have each input bilingual dictionary as
	 * one large table (with lots of LI redundancies), OR have them as
	 * normalised tables, but create an "all-in" view for each bilingual dictionary. 
	 */
	static String get_lang1_LI_qstr;
	static String lang1_lang2_qstr;
	static String lang2_lang3_qstr;
	static String lang3_lang2_qstr;
	
	static String lang1_lang2_dict_table;
	static String lang1_lang2_LI_col;
	static String lang1_lang2_gloss_id_col;
	static String lang1_lang2_gloss_col;
	static String lang1_lang2_pos_col;
	static String lang1_lang2_qsuffix;

	static String lang2_lang3_dict_table;
	static String lang2_lang3_LI_col;
	static String lang2_lang3_gloss_id_col;
	static String lang2_lang3_gloss_col;
	static String lang2_lang3_pos_col;
	static String lang2_lang3_qsuffix;
	
	static String lang3_lang2_dict_table;
	static String lang3_lang2_LI_col;
	static String lang3_lang2_gloss_id_col;
	static String lang3_lang2_gloss_col;
	static String lang3_lang2_pos_col;
	static String lang3_lang2_qsuffix;

	static PreparedStatement lang1_lang2_Query;
	static PreparedStatement lang2_lang3_Query;
	static PreparedStatement lang3_lang2_Query;
	static PreparedStatement addTripleStmt;
	
	static double threshold_ALPHA;
	static double threshold_BETA; // ad-hoc magic number :S
	
	/* The new language to be added */
	static String new_lang;


	protected static void initParams(ResourceBundle bundle) {
		triplesPrefix = bundle.getString("triplesPrefix");
		logPrefix = bundle.getString("logPrefix");

		lang1 = bundle.getString("lang1");
		lang2 = bundle.getString("lang2");
		lang3 = bundle.getString("lang3");
		

		lang1_lang2_Dict = DictSource.valueOf(bundle.getString("lang1_lang2_Dict"));
		lang2_lang3_Dict = DictSource.valueOf(bundle.getString("lang2_lang3_Dict"));
		lang3_lang2_Dict = DictSource.valueOf(bundle.getString("lang3_lang2_Dict"));
		lang1_lang2_DictPrefix = bundle.getString("lang1_lang2_DictPrefix");
		lang2_lang3_DictPrefix = bundle.getString("lang2_lang3_DictPrefix");
		lang3_lang2_DictPrefix = bundle.getString("lang3_lang2_DictPrefix");

		dbHost = bundle.getString("dbHost");
		dbPort = Integer.parseInt(bundle.getString("dbPort"));
		dbName = bundle.getString("dbName");
		dbUsername = bundle.getString("dbUsername");
		dbPassword = bundle.getString("dbPassword");

		lang1_lang2_dict_table = bundle.getString("lang1_lang2_dict_table");
		lang1_lang2_LI_col = bundle.getString("lang1_lang2_LI_col");
		lang1_lang2_gloss_id_col = bundle.getString("lang1_lang2_gloss_id_col");
		lang1_lang2_gloss_col = bundle.getString("lang1_lang2_gloss_col");
		lang1_lang2_pos_col = bundle.getString("lang1_lang2_pos_col");
		lang1_lang2_qsuffix = bundle.getString("lang1_lang2_qsuffix");
		
		get_lang1_LI_qstr = "SELECT DISTINCT `" + lang1_lang2_LI_col + "` FROM `" +
				lang1_lang2_dict_table + "`";
		lang1_lang2_qstr = "SELECT * FROM `" + lang1_lang2_dict_table+ "` WHERE " +
				lang1_lang2_LI_col + "= ? " +
				((lang1_lang2_qsuffix == null || lang1_lang2_qsuffix.trim().length() == 0) ? 
						"" : lang1_lang2_qsuffix);

		lang2_lang3_dict_table = bundle.getString("lang2_lang3_dict_table");
		lang2_lang3_LI_col = bundle.getString("lang2_lang3_LI_col");
		lang2_lang3_gloss_id_col = bundle.getString("lang2_lang3_gloss_id_col");
		lang2_lang3_gloss_col = bundle.getString("lang2_lang3_gloss_col");
		lang2_lang3_pos_col = bundle.getString("lang2_lang3_pos_col");
		lang2_lang3_qsuffix = bundle.getString("lang2_lang3_qsuffix");
		
		lang2_lang3_qstr = "SELECT * FROM `" + lang2_lang3_dict_table+ "` WHERE " +
				lang2_lang3_LI_col + "= ? " +
				((lang2_lang3_qsuffix == null || lang2_lang3_qsuffix.trim().length() == 0) ? 
						"" : lang2_lang3_qsuffix);

		lang3_lang2_dict_table = bundle.getString("lang3_lang2_dict_table");
		lang3_lang2_LI_col = bundle.getString("lang3_lang2_LI_col");
		lang3_lang2_gloss_id_col = bundle.getString("lang3_lang2_gloss_id_col");
		lang3_lang2_gloss_col = bundle.getString("lang3_lang2_gloss_col");
		lang3_lang2_pos_col = bundle.getString("lang3_lang2_pos_col");
		lang3_lang2_qsuffix = bundle.getString("lang3_lang2_qsuffix");

		lang3_lang2_qstr = "SELECT * FROM `" + lang3_lang2_dict_table+ "` WHERE " +
				lang3_lang2_LI_col + "= ? " +
				((lang3_lang2_qsuffix == null || lang3_lang2_qsuffix.trim().length() == 0) ? 
						"" : lang3_lang2_qsuffix);
		
		threshold_ALPHA = Double.parseDouble(bundle.getString("threshold_ALPHA"));
		threshold_BETA = Double.parseDouble(bundle.getString("threshold_BETA"));

		new_lang = bundle.getString("new_lang");
	}

	protected static void connectDB() throws SQLException {
		dbConn = DBUtils.getUTF8MySQLConnection(dbHost, dbPort, dbName,
				dbUsername, dbPassword);
		stmt = dbConn.createStatement();
		stmt.execute("SET NAMES UTF8");
		stmt.execute("SET FOREIGN_KEY_CHECKS = 0");

	}
	
	protected static void disconnectDB() throws SQLException {
		stmt.close();
		dbConn.close();
	}

}
