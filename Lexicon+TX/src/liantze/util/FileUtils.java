/* @(#)FileTools.java      1.0   May 2006
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */

package liantze.util;

import java.io.*;

/**
 * This a convenience class for supporting reading from and writing to UTF-8
 * encoded files, including XML.
 * 
 * @author LIM Lian Tze
 * @version 1.0
 * 
 */
public class FileUtils {
    
    /**
     * This class is a utility rather than a class and therefore should never be
     * instantiated.
     * 
     */
    private FileUtils() {
    }


    /**
     * Gets a buffered file reader using UTF-8 encoding.
     * 
     * @since utf8tools 1.0
     * @param filename
     *            The file to read from
     * @return BufferedReader from <code>filename</code> using UTF-8 encoding.
     */
    public static BufferedReader getUTF8Reader(String filename) {
    	return getReader(filename, "UTF-8");
    }
    
    public static BufferedReader getReader(String filename, String charset) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(new File(filename)), charset));
        } catch (UnsupportedEncodingException e) {
            System.err.println(charset + " encoding not supported: ");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " not found: ");
            e.printStackTrace();
        }
        return reader;    	
    }
    

    /**
     * Gets a plain text file writer using UTF-8 encoding. Existing files with
     * the same name will be overwritten!
     * 
     * @since utf8tools 1.0
     * @param filename
     *            The file to write to.
     * @return PrintWriter to <code>filename</code> using UTF-8 encoding.
     */
    public static PrintWriter getUTF8Writer(String filename) {
    	return getWriter(filename, "UTF-8");
    }
    
    public static PrintWriter getWriter(String filename, String charset) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream(new File(filename)), charset), true);
        } catch (UnsupportedEncodingException e) {
            System.err.println(charset + " encoding not supported: ");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " not found: ");
            e.printStackTrace();
        }
        return writer;
    }

    /**
     * Reads the entire contents of a file into a string.
     * @param filename The file to read from.
     * @return The entire contents of <code>filename</code> as a string.
     */
    public static String readTextFile(String filename) {
        StringBuffer sb = new StringBuffer(1024);
        BufferedReader reader = FileUtils.getUTF8Reader(filename);

        char[] chars = new char[1024];
        int numRead = 0;
        try {
            while ((numRead = reader.read(chars)) > -1) {
                sb.append(String.valueOf(chars));
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
    
    /**
     * Convenience method for writing to a file.
     * 
     * @param S the String to write
     * @param filename File to output to.
     * @param append true id data have to be append to the file
     */
    public static void print(String S, String filename, boolean append) {
        try {
            PrintWriter writer = new PrintWriter(new FileOutputStream(filename, append));
            writer.println(S);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
