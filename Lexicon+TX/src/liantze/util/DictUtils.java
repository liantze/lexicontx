/* @(#)DictUtils.java      1.1   July 2012
 * WARNING: HORRIBLE GRAD STUDENT'S CODE. USE AT YOUR OWN PERIL.
 */
package liantze.util;

/**
 * Bunch of formatting/etc routines that don't belong anywhere else...?
 * 
 * 
 * @author LIM Lian Tze (liantze@gmail.com)
 *
 */
public class DictUtils {
	
	// TODO: implement normalisation for your language here if necessary
	/**
	 * Normalises a gloss in a particular language to get a more
	 * lemma-like form.
	 * @param lang
	 * @param gloss
	 * @return
	 */
	public static String normalise(String lang, String gloss){
		// English
		if (lang.equals("eng")) {
			return enNormalise(gloss);
		}
		//Chinese
		if (lang.equals("zho")) {
			return zhNormalise(gloss);
		}
		
		// languages not yet implemented: return same gloss
		return gloss;
	}
	
	/**
	 * Normalises or massages some common form of Chinese glosses
	 * to get a lemma-like form. Non-exhaustive.
	 * @param zhGloss
	 * @return
	 */
	public static String zhNormalise(String zhGloss) {
		String s = zhGloss;
		if (s.matches("\\(.+的缩写\\)")) {
			return s.substring(1,s.length()-4).trim();
		}
		if (s.matches("\\(.+\\)")) {
			return s.substring(1, s.length()-1).trim();
		}
		if (s.matches("(\\(.+\\)).+")){
			return s.substring(s.indexOf('(')+1);
		}
		return s;
	}

	public static String msNormalise(String s) {
		return s;
	}

	/**
	 * Normalises an English gloss to get a more
	 * lemma-like form.
	 * @param s
	 * @return
	 */
	public static String enNormalise(String s) {
		return s.replaceFirst("to be ", "").replaceFirst("to ", "").replaceAll("\\(classifier\\)", "").trim();
	}

}
